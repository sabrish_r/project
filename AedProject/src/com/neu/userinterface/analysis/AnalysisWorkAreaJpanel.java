/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.userinterface.analysis;


import com.neu.business.enterprise.Enterprise;
import com.neu.business.network.Network;
import com.neu.business.useraccount.UserAccount;
import com.neu.userinterface.LoginJPanel;
import com.neu.business.vehicle.VehiclePart;
import com.neu.business.vehicle.VehiclePartCondition;
import com.neu.business.workqueue.VehicleServiceRequest;
import com.neu.business.workqueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author Sabrish
 */
public class AnalysisWorkAreaJpanel extends javax.swing.JPanel {

    /**
     * Creates new form AnalysisWorkAreaJpanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Network network;

    public AnalysisWorkAreaJpanel(JPanel userProcessContainer, UserAccount userAccount, Network network) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.network = network;
        this.userAccount = userAccount;
        updateView();
    }

    private void updateView() {

        enterpriseTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    onRowSelectionChanged();
                }
            }
        });

        networkName.setText(network.getName().toUpperCase() + " ANALYSIS");
        populateEnterpriseTable();
    }

    private void onRowSelectionChanged() {
        int selectedItem = enterpriseTable.getSelectedRow();

        if (selectedItem > -1) {

            Enterprise enterprise = (Enterprise) enterpriseTable.getValueAt(selectedItem, 0);
            if (enterprise != null) {
                statusLabel.setText(enterprise.getName().toUpperCase() + " VEHICLE PART ANALYTICS");
                populatePieChart(enterprise);
            }

        }

    }

    private void populatePieChart(Enterprise enterprise) {
        if (enterprise != null) {
            ArrayList<WorkRequest> workRequests = enterprise.getAllAnalyticsRequest();
            if (workRequests != null && workRequests.size() > 0) {
                ArrayList<VehiclePart> brakeParts = new ArrayList<>();
                ArrayList<VehiclePart> acParts = new ArrayList<>();
                ArrayList<VehiclePart> carboratorParts = new ArrayList<>();
                ArrayList<VehiclePart> pollutionParts = new ArrayList<>();
                ArrayList<VehiclePart> engineParts = new ArrayList<>();
                for (WorkRequest workRequest : workRequests) {
                    if (workRequest instanceof VehicleServiceRequest) {
                        VehicleServiceRequest vehicleServiceRequest = (VehicleServiceRequest) workRequest;
                        Map<VehiclePart, VehiclePartCondition> vehiclePartCondition = vehicleServiceRequest.getResolvedVehiclePartConditions();
                        if (vehiclePartCondition != null && !vehiclePartCondition.isEmpty()) {

                            for (VehiclePart vehiclePart : vehiclePartCondition.keySet()) {
                                if (vehiclePart == VehiclePart.BRAKE) {
                                    brakeParts.add(vehiclePart);
                                } else if (vehiclePart == VehiclePart.AC) {
                                    acParts.add(vehiclePart);
                                } else if (vehiclePart == VehiclePart.POLLUTION_CONTROL) {
                                    pollutionParts.add(vehiclePart);
                                } else if (vehiclePart == VehiclePart.CARBORATOR) {
                                    carboratorParts.add(vehiclePart);
                                } else if (vehiclePart == VehiclePart.ENGINE) {
                                    engineParts.add(vehiclePart);
                                }
                            }
                        }
                    }
                }

                DefaultPieDataset dataset = new DefaultPieDataset();
                dataset.setValue("Brake", new Double(brakeParts.size()));
                dataset.setValue("AC", new Double(acParts.size()));
                dataset.setValue("Pollution Part", new Double(pollutionParts.size()));
                dataset.setValue("Carborator", new Double(carboratorParts.size()));
                dataset.setValue("Engine", new Double(engineParts.size()));
                JFreeChart chart = createChart(dataset);
                analysisFrame.setContentPane(new ChartPanel(chart));
            }
        }
    }

    private JFreeChart createChart(PieDataset dataset) {

        JFreeChart chart = ChartFactory.createPieChart(
                "NO. OF ISSUE IN EACH PART", // chart title
                dataset, // data
                true, // include legend
                true,
                false
        );

        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
        plot.setNoDataMessage("No data available");
        plot.setCircular(false);
        plot.setLabelGap(0.02);
        return chart;

    }

    private void populateEnterpriseTable() {
        if (network != null && network.getEnterpriseDirectory().getEnterpriseList().size() > 0) {
            DefaultTableModel defaultTableModel = (DefaultTableModel) enterpriseTable.getModel();
            defaultTableModel.setRowCount(0);
            visibleUI(true);
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (enterprise.getEnterpriseType() == Enterprise.EnterpriseType.LOCATION_CENTRE) {
                    Object[] row = new Object[1];
                    row[0] = enterprise;
                    defaultTableModel.addRow(row);
                }              
            }
            
            if(network.getEnterpriseDirectory().getEnterpriseList().size() > 0) {
                enterpriseTable.setRowSelectionInterval(0, 0);
               }
           
           if(network.getEnterpriseDirectory().getEnterpriseList().isEmpty()){
               visibleUI(false);
               
            }
        }else{
                visibleUI(false);
            }
    }

    private void visibleUI(boolean visible) {
        statusLabel.setVisible(visible);
        analysisFrame.setVisible(visible);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        networkName = new javax.swing.JLabel();
        logoutButton = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        enterpriseTable = new javax.swing.JTable();
        jSeparator2 = new javax.swing.JSeparator();
        analysisFrame = new javax.swing.JInternalFrame();
        statusLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(53, 111, 152));
        setLayout(new java.awt.GridBagLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        networkName.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        networkName.setForeground(new java.awt.Color(162, 162, 162));
        networkName.setText("NETWORK ANALYSIS");

        logoutButton.setBackground(new java.awt.Color(51, 153, 255));
        logoutButton.setForeground(new java.awt.Color(255, 255, 255));
        logoutButton.setText("LOGOUT");
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutButtonActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(162, 162, 162));
        jLabel4.setText("ENTERPRISE");

        enterpriseTable.setForeground(new java.awt.Color(162, 162, 162));
        enterpriseTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "ENTERPRISE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        enterpriseTable.setSelectionBackground(new java.awt.Color(255, 255, 255));
        enterpriseTable.setSelectionForeground(new java.awt.Color(162, 162, 162));
        jScrollPane1.setViewportView(enterpriseTable);

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setPreferredSize(new java.awt.Dimension(10, 50));

        analysisFrame.setBackground(new java.awt.Color(255, 255, 255));
        analysisFrame.setVisible(true);

        javax.swing.GroupLayout analysisFrameLayout = new javax.swing.GroupLayout(analysisFrame.getContentPane());
        analysisFrame.getContentPane().setLayout(analysisFrameLayout);
        analysisFrameLayout.setHorizontalGroup(
            analysisFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 380, Short.MAX_VALUE)
        );
        analysisFrameLayout.setVerticalGroup(
            analysisFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 504, Short.MAX_VALUE)
        );

        statusLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        statusLabel.setForeground(new java.awt.Color(162, 162, 162));
        statusLabel.setText("VEHICLE PART STATUS");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(analysisFrame, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(207, 207, 207))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(337, 337, 337)
                .addComponent(networkName)
                .addGap(360, 360, 360)
                .addComponent(logoutButton)
                .addGap(46, 46, 46))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(23, 23, 23)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jSeparator3)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(80, 80, 80)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 609, Short.MAX_VALUE)))
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(networkName)
                    .addComponent(logoutButton))
                .addGap(31, 31, 31)
                .addComponent(statusLabel)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(analysisFrame, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addContainerGap())
                        .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE))))
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 62, 66, 0);
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void logoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutButtonActionPerformed
        userProcessContainer.removeAll();
        LoginJPanel loginJPanel = new LoginJPanel(userProcessContainer);
        userProcessContainer.add("LoginJPanel", loginJPanel);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.next(userProcessContainer);
    }//GEN-LAST:event_logoutButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JInternalFrame analysisFrame;
    private javax.swing.JTable enterpriseTable;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JButton logoutButton;
    private javax.swing.JLabel networkName;
    private javax.swing.JLabel statusLabel;
    // End of variables declaration//GEN-END:variables
}
