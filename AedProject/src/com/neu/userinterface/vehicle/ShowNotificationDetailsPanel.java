/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.userinterface.vehicle;

import com.neu.business.ConfigureASystem;
import com.neu.business.IManagerDelegate;
import com.neu.business.enterprise.Enterprise;
import com.neu.business.manager.BroadcastMessage;
import com.neu.business.manager.BroadcastStateKey;
import com.neu.business.manager.BroadcastValue;
import com.neu.business.manager.IBroadcastObserver;
import com.neu.business.network.Network;
import com.neu.business.organization.Organization;
import com.neu.business.organization.ServiceCentreOrganization;
import com.neu.business.organization.VehicleOrganization;
import com.neu.business.useraccount.UserAccount;
import com.neu.utils.Toast;
import com.neu.utils.Validation;
import com.neu.business.vehicle.Address;
import com.neu.business.vehicle.Location;
import com.neu.business.vehicle.Notification;
import com.neu.business.vehicle.Vehicle;
import com.neu.business.vehicle.VehiclePart;
import com.neu.business.vehicle.VehiclePartCondition;
import com.neu.business.workqueue.RequestStatus;
import com.neu.business.workqueue.VehicleRequestType;
import com.neu.business.workqueue.VehicleServiceRequest;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sabrish
 */
public class ShowNotificationDetailsPanel extends javax.swing.JPanel implements IBroadcastObserver{

    /**
     * Creates new form ShowNotificationDetails
     */
    private JPanel userProcessContainer;
    private UserAccount vehicleAccount;
    private Vehicle vehicle;
    private Enterprise enterprise;
    private Network network;
    private Notification notification;

    public ShowNotificationDetailsPanel(JPanel userProcessContainer, UserAccount vehicleAccount, Enterprise enterprise, Network network, Notification notification) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.vehicleAccount = vehicleAccount;
        this.vehicle = (Vehicle) vehicleAccount.getPerson();
        this.enterprise = enterprise;
        this.network = network;
        this.notification = notification;
        errorLabel1.setVisible(false);
        updateView();
        updateLocation();
        IManagerDelegate managerDelegate = ConfigureASystem.getInstance().getManagerDelegate();
        managerDelegate.registerBroadcastObserver(this);
        addContainerListener(new ContainerAdapter() {

            @Override
            public void componentRemoved(ContainerEvent e) {
                managerDelegate.unregisterBroadcastObserver(ShowNotificationDetailsPanel.this);
            }

        });
    }

    private void updateLocation() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                updateVehicleLocation();
            }
        }).start();
    }

    private void updateVehicleLocation() {

        if (vehicle.getLocation() != null && vehicle.getLocation().getLatitude() != null && vehicle.getLocation().getLongitute() != null) {
            try {
                String imageUrl
                        = "http://maps.googleapis.com/maps/api/staticmap?zoom=18&size=400x400&markers=" + vehicle.getLocation().getLatitude() + "," + vehicle.getLocation().getLongitute() + "&sensor=true";
                String destinationFile = "image.jpg";

                URL url = new URL(imageUrl);
                InputStream is = url.openStream();
                OutputStream os = new FileOutputStream(destinationFile);

                byte[] b = new byte[2048];
                int length;

                while ((length = is.read(b)) != -1) {
                    os.write(b, 0, length);
                }

                is.close();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
            mapLabel.setIcon(new ImageIcon((new ImageIcon("image.jpg")).getImage()
                    .getScaledInstance(400, 400, java.awt.Image.SCALE_SMOOTH)));
            mapLabel.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        JFrame mapFrame = new JFrame();
                        Browser browser = new Browser();
                        BrowserView browserView = new BrowserView(browser);
                        browserView.setPreferredSize(new Dimension(400, 350));
                        mapFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                        mapFrame.setSize(800, 600);
                        mapFrame.setLocationRelativeTo(null);
                        mapFrame.setVisible(true);
                        mapFrame.add(browserView);
                        browser.addLoadListener(new LoadAdapter() {

                            @Override
                            public void onFinishLoadingFrame(FinishLoadingEvent fle) {
                                super.onFinishLoadingFrame(fle);
                                Address destLocation = enterprise.getAddress();
                                Address startLocation = vehicle.getAddress();

                                String start = startLocation.getAddress1() + "," + startLocation.getCity() + "," + startLocation.getState() + "," + startLocation.getZipcode();
                                // dest = dest.replace(" ", "+");
                                // start = start.replace(" ", "+");
                              //  String dest = destLocation.getAddress1() + "," + destLocation.getCity() + "," + destLocation.getState() + "," + destLocation.getZipcode();
                              //  browser.executeJavaScript("addDestination('" + dest + "')");
                                showAllEnterpriseInMap(browser);
                                //  browser.executeJavaScript("addDestination('"+"Boston"+"')");
                                browser.executeJavaScript("addStart('" + start + "')");
                                //   browser.executeJavaScript("addStart('"+"Newyork"+"')");
                                browser.removeLoadListener(this);

                            }

                        });

                        browser.loadURL("D:\\NorthEasternStudy\\AED\\Project\\project\\AedProject\\maps\\maps.html");
                    }
                }
            });
        }
    }

    private void showAllEnterpriseInMap(Browser browser) {
        if (browser != null && notification != null) {
            for (Location location : notification.getLocations()) {              
                    Address destLocation = location.getAddress();
                    String dest = destLocation.getAddress1() + "," + destLocation.getCity() + "," + destLocation.getState() + "," + destLocation.getZipcode();
                    browser.executeJavaScript("addDestination('" + dest + "')");               
            }
        }

    }

    private void updateView() {
        upcomingLabel.setText(vehicle.getUpcomingAppointment() == null ? "No upcoming appointment" : vehicle.getUpcomingAppointment().toString());
        if(notification != null){
            messageLabel.setText(notification.getMessage());
        }
        populateTable();

    }

    private void populateTable() {

        if (vehicle != null) {
            Map<VehiclePart, VehiclePartCondition> vehiclePartConditions = vehicle.getVehiclePartConditions();
            if (vehiclePartConditions != null && vehiclePartConditions.size() > 0) {
                DefaultTableModel defaultTableModel = (DefaultTableModel) conditionTable.getModel();
                defaultTableModel.setRowCount(0);
                for (Map.Entry<VehiclePart, VehiclePartCondition> entrySet : vehiclePartConditions.entrySet()) {
                    Object[] row = new Object[2];
                    row[0] = entrySet.getValue();
                    row[1] = entrySet.getValue().getCondition();
                    defaultTableModel.addRow(row);
                }
            }
        }

    }

    private void showAppointmentDialog() {

        if (isValidationSuccessful()) {
            errorLabel1.setVisible(false);
            String text = messageTextArea.getText();
            VehicleServiceRequest vehicleServiceRequest = new VehicleServiceRequest();
            vehicleServiceRequest.setSender(vehicleAccount);
            vehicleServiceRequest.setVehicleRequestType(VehicleRequestType.SERVICE);
            vehicleServiceRequest.setMessage(text);
            vehicleServiceRequest.setRequestDate(new Date());
            vehicleServiceRequest.setRequestStatus(RequestStatus.REQUEST_RAISED);
            vehicleAccount.getWorkQueue().addWorkRequest(vehicleServiceRequest);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Integer.parseInt(yyTextField.getText()), Integer.parseInt(mmTextField.getText()) - 1,
                    Integer.parseInt(ddTextField.getText()), Integer.parseInt(hrTextField.getText()), Integer.parseInt(minTextField.getText()));
            vehicle.setUpcomingAppointment(calendar.getTime());
            if (enterprise != null && enterprise.getOrganizationDirectory().getOrganizationList() != null
                    && enterprise.getOrganizationDirectory().getOrganizationList().size() > 0) {

                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    if ((organization instanceof ServiceCentreOrganization) || (organization instanceof VehicleOrganization)) {
                        organization.getWorkQueue().addWorkRequest(vehicleServiceRequest);
                    }
                }
            }
            upcomingLabel.setText(vehicle.getUpcomingAppointment() == null ? "No upcoming appointment" : vehicle.getUpcomingAppointment().toString());
            Toast.makeText(new JFrame(), "Appointment created successfully", Toast.LENGTH_SHORT, Toast.Style.SUCCESS).display();
            Map<BroadcastStateKey, Object> map = new HashMap<>();
            map.put(BroadcastStateKey.BROADCAST_MESSAGE, BroadcastValue.VEHICLE_STATUS_UPDATED);
            ConfigureASystem.getInstance().getManagerDelegate().sendBroadCast(map);
        }
    }

    private boolean isValidationSuccessful() {

        if (!Validation.isMonthValid(mmTextField.getText())) {
            showError("Please enter valid month");
            return false;
        } else if (!Validation.isDateValid(ddTextField.getText())) {
            showError("Please enter valid date");
            return false;
        } else if (!Validation.isFutureYearValid(yyTextField.getText())) {
            showError("Please enter valid year");
            return false;
        } else if (!Validation.isHourValid(hrTextField.getText())) {
            showError("Please select time 8:00 to 19:59");
            return false;
        } else if (!Validation.isMinuteValid(minTextField.getText())) {
            showError("Please select minute 0 to 59");
            return false;
        } else if (!(messageTextArea != null && messageTextArea.getText().length() > 0)) {
            showError("Please enter message");
            return false;
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Integer.parseInt(yyTextField.getText()), Integer.parseInt(mmTextField.getText()),
                    Integer.parseInt(ddTextField.getText()), Integer.parseInt(hrTextField.getText()), Integer.parseInt(minTextField.getText()));
            if (!Validation.isDateFuture(calendar.getTime())) {
                showError("Please enter future date");
                return false;
            }
        }

        return true;
    }

    private void showError(String a_text) {
        errorLabel1.setText(a_text);
        errorLabel1.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        upcomingLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        conditionTable = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        apppointmentButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        mapLabel = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        mmTextField = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        ddTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        yyTextField = new javax.swing.JTextField();
        hrTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        minTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        messageTextArea = new javax.swing.JTextArea();
        errorLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        messageLabel = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(23, 111, 152));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(886, 535));

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(162, 162, 162));
        jLabel1.setText("NOTIFICATION");

        jLabel2.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(162, 162, 162));
        jLabel2.setText("Upcoming Appointment    : ");

        upcomingLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        upcomingLabel.setForeground(new java.awt.Color(162, 162, 162));
        upcomingLabel.setText("11/12/2013 4:30 PM");

        conditionTable.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        conditionTable.setForeground(new java.awt.Color(162, 162, 162));
        conditionTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "PART", "CONDITION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(conditionTable);

        jLabel4.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(162, 162, 162));
        jLabel4.setText("CONDITION");

        apppointmentButton.setBackground(new java.awt.Color(55, 153, 255));
        apppointmentButton.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        apppointmentButton.setForeground(new java.awt.Color(255, 255, 255));
        apppointmentButton.setText("FIX APPOINTMENT");
        apppointmentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                apppointmentButtonActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(162, 162, 162));
        jLabel3.setText("Vehicle Location");

        mapLabel.setBackground(new java.awt.Color(102, 102, 255));
        mapLabel.setText("MAP");

        jLabel5.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(162, 162, 162));
        jLabel5.setText("Date");

        mmTextField.setForeground(new java.awt.Color(162, 162, 162));
        mmTextField.setToolTipText("MM");

        jLabel8.setForeground(new java.awt.Color(162, 162, 162));
        jLabel8.setText("/");

        ddTextField.setForeground(new java.awt.Color(162, 162, 162));
        ddTextField.setToolTipText("DD");

        jLabel9.setForeground(new java.awt.Color(162, 162, 162));
        jLabel9.setText("/");

        yyTextField.setForeground(new java.awt.Color(162, 162, 162));
        yyTextField.setToolTipText("YYYY");

        hrTextField.setForeground(new java.awt.Color(162, 162, 162));
        hrTextField.setToolTipText("MM");

        jLabel10.setForeground(new java.awt.Color(162, 162, 162));
        jLabel10.setText(":");

        minTextField.setForeground(new java.awt.Color(162, 162, 162));
        minTextField.setToolTipText("DD");

        jLabel6.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(162, 162, 162));
        jLabel6.setText("MESSAGE");

        messageTextArea.setColumns(20);
        messageTextArea.setRows(5);
        jScrollPane3.setViewportView(messageTextArea);

        errorLabel1.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        errorLabel1.setForeground(new java.awt.Color(255, 0, 0));
        errorLabel1.setText("Error");

        jLabel7.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(162, 162, 162));
        jLabel7.setText("MESSAGE : ");

        messageLabel.setBackground(new java.awt.Color(162, 162, 162));
        messageLabel.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        messageLabel.setForeground(new java.awt.Color(162, 162, 162));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(errorLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(78, 78, 78))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(47, 47, 47)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(mmTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ddTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(yyTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(hrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(minTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel4)
                            .addComponent(apppointmentButton)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(upcomingLabel))
                            .addComponent(jLabel7)
                            .addComponent(messageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(60, 60, 60)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(mapLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(40, 40, 40))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(36, 36, 36)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(upcomingLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(messageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(minTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10)
                                    .addComponent(hrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(yyTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9)
                                    .addComponent(ddTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8)
                                    .addComponent(mmTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(76, 76, 76)
                                .addComponent(jLabel6)
                                .addGap(78, 78, 78)
                                .addComponent(apppointmentButton))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(50, 50, 50)
                        .addComponent(mapLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(errorLabel1)
                .addGap(21, 21, 21))
        );

        backButton.setBackground(new java.awt.Color(255, 255, 255));
        backButton.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        backButton.setForeground(new java.awt.Color(23, 111, 152));
        backButton.setText("<<");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(64, 64, 64)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 903, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(backButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(backButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 562, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void apppointmentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_apppointmentButtonActionPerformed

        showAppointmentDialog();
    }//GEN-LAST:event_apppointmentButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton apppointmentButton;
    private javax.swing.JButton backButton;
    private javax.swing.JTable conditionTable;
    private javax.swing.JTextField ddTextField;
    private javax.swing.JLabel errorLabel1;
    private javax.swing.JTextField hrTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel mapLabel;
    private javax.swing.JLabel messageLabel;
    private javax.swing.JTextArea messageTextArea;
    private javax.swing.JTextField minTextField;
    private javax.swing.JTextField mmTextField;
    private javax.swing.JLabel upcomingLabel;
    private javax.swing.JTextField yyTextField;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onNotificationState(BroadcastMessage a_NotificationState, Map<BroadcastStateKey, Object> a_Parameters) {
        
    }
}
