/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.userinterface.vehicle;

import com.neu.business.ConfigureASystem;
import com.neu.business.IManagerDelegate;
import com.neu.business.enterprise.Enterprise;
import com.neu.business.manager.BroadcastMessage;
import com.neu.business.manager.BroadcastStateKey;
import com.neu.business.manager.BroadcastValue;
import com.neu.business.manager.IBroadcastObserver;
import com.neu.business.useraccount.UserAccount;
import com.neu.business.workqueue.RequestStatus;
import com.neu.business.workqueue.VehicleServiceRequest;
import com.neu.business.workqueue.WorkRequest;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sabrish
 */
public class VehicleRequestPanel extends javax.swing.JPanel implements IBroadcastObserver{

    /**
     * Creates new form VehicleRequestPanel
     */
    private JPanel userProcessContainer;
    private UserAccount vehicle;
    private Enterprise enterprise;
    private IManagerDelegate managerDelegate;
    
    public VehicleRequestPanel(JPanel userProcessContainer, UserAccount vehicle, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.vehicle = vehicle;
        this.enterprise = enterprise;
        managerDelegate = ConfigureASystem.getInstance().getManagerDelegate();
        managerDelegate.registerBroadcastObserver(this);
       addContainerListener(new ContainerAdapter() {

            @Override
            public void componentRemoved(ContainerEvent e) {
                managerDelegate.unregisterBroadcastObserver(VehicleRequestPanel.this);
            }
                
                
            });
       // updateView();
        populateMyWorkRequestTable();
        populatePendingRequestTable();
    }
    
    private void updateView(){
          workRequestTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable target = (JTable) e.getSource();
                    int row = target.getSelectedRow();
                    if(row > -1){                      
                      VehicleServiceRequest serviceRequest = (VehicleServiceRequest)vehicle.getWorkQueue().getWorkRequestList().get(row);
                       if(serviceRequest != null && serviceRequest.getSender() != vehicle){
                           showConfirmationDialog(serviceRequest);
                       }
                    }
                }
            }
        });
    }
    
    private void showConfirmationDialog( VehicleServiceRequest serviceRequest ){
        
        int selectedOption = JOptionPane.showConfirmDialog(null, "Would you like to confirm your appointment", "Appointment", JOptionPane.YES_NO_OPTION);
        if(selectedOption == JOptionPane.YES_OPTION){            
            serviceRequest.setStatus("Appointment Fixed");
        }
        
    }
    
    private void populatePendingRequestTable(){
        
        if(vehicle != null && vehicle.getWorkQueue() != null){
            DefaultTableModel defaultTableModel = (DefaultTableModel) pendingWorkRequestTable.getModel();
            defaultTableModel.setRowCount(0);
            for(WorkRequest serviceRequest : vehicle.getWorkQueue().getWorkRequestList() ){
                VehicleServiceRequest vehicleServiceRequest = (VehicleServiceRequest)serviceRequest;
                if(((vehicleServiceRequest.getRequestStatus() == RequestStatus.REQUEST_RAISED) && (vehicleServiceRequest.getSender() != vehicle) ) || 
                        vehicleServiceRequest.getRequestStatus() == RequestStatus.REQUEST_PENDING && (vehicleServiceRequest.getSender() != vehicle)){
                 Object[] row = new Object[7];
                 row[0] = vehicleServiceRequest;
                 row[1] = vehicleServiceRequest.getRequestStatus();
                 row[2] = vehicleServiceRequest.getSender();             
                 row[3] = vehicleServiceRequest.getMessage();
                 row[4] = vehicleServiceRequest.getRequestDate();
                 row[5] = vehicleServiceRequest.getResolveDate()==null?"Pending":vehicleServiceRequest.getResolveDate();
                 row[6] = false;
                 defaultTableModel.addRow(row);
                }
            }
            
            pendingWorkRequestTable.getModel().addTableModelListener(new TableModelListener() {
              @Override
              public void tableChanged(TableModelEvent e) {
                  for(int i=0;i<pendingWorkRequestTable.getModel().getRowCount();i++)
                  {
                    if ((Boolean) pendingWorkRequestTable.getModel().getValueAt(i,6))
                    {  
                      enablePendingButton(true);
                      break;
                    }else {
                        enablePendingButton(false);
                    }
                 }     
              }
            });
            
        }
    }
    
    private void enablePendingButton(boolean enable){
        
        pendingDeleteButton.setEnabled(enable);
        acceptPendingButton.setEnabled(enable);
        rejectPendingButton.setEnabled(enable);
    }
    
    private void populateMyWorkRequestTable(){
        
        if(vehicle != null && vehicle.getWorkQueue() != null){
            DefaultTableModel defaultTableModel = (DefaultTableModel) workRequestTable.getModel();
            defaultTableModel.setRowCount(0);
            for(WorkRequest serviceRequest : vehicle.getWorkQueue().getWorkRequestList() ){
                VehicleServiceRequest vehicleServiceRequest = (VehicleServiceRequest)serviceRequest;
                if((vehicleServiceRequest.getRequestStatus() == RequestStatus.REQUEST_ACCEPTED) || (vehicleServiceRequest.getSender()== vehicle)){
                 Object[] row = new Object[8];
                 row[0] = vehicleServiceRequest;
                 row[1] = vehicleServiceRequest.getRequestStatus();
                 row[2] = vehicleServiceRequest.getSender();
                 row[3] = vehicleServiceRequest.getReceiver();
                 row[4] = vehicleServiceRequest.getMessage();
                 row[5] = vehicleServiceRequest.getRequestDate();
                 row[6] = vehicleServiceRequest.getResolveDate()==null?"Pending":vehicleServiceRequest.getResolveDate();
                 row[7] = false;
                 defaultTableModel.addRow(row);
                }
            }
            
             workRequestTable.getModel().addTableModelListener(new TableModelListener() {
              @Override
              public void tableChanged(TableModelEvent e) {
                  for(int i=0;i<workRequestTable.getModel().getRowCount();i++)
                  {
                    if ((Boolean) workRequestTable.getModel().getValueAt(i,7))
                    {  
                     enableMyRequestButton(true);
                      break;
                    }else{
                        enableMyRequestButton(false);
                    }
                 }     
              }
            });
        }
    }
    
    private void enableMyRequestButton(boolean enable){
        
        deleteMyButton.setEnabled(enable);
        rejectMyButton.setEnabled(enable);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        pendingWorkRequestTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        pendingDeleteButton = new javax.swing.JButton();
        acceptPendingButton = new javax.swing.JButton();
        rejectPendingButton = new javax.swing.JButton();
        deleteMyButton = new javax.swing.JButton();
        rejectMyButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(162, 162, 162));
        jLabel1.setText("MY REQUEST");

        workRequestTable.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        workRequestTable.setForeground(new java.awt.Color(162, 162, 162));
        workRequestTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "REQUEST", "STATUS", "SENDER", "RECEIVER", "MESSAGE", "REQUEST DATE", "RESOLVED DATE", "SELECT"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        workRequestTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(workRequestTable);

        pendingWorkRequestTable.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        pendingWorkRequestTable.setForeground(new java.awt.Color(162, 162, 162));
        pendingWorkRequestTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "REQUEST", "STATUS", "SENDER", "MESSAGE", "REQUEST DATE", "RESOLVE DATE", "SELECT"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        pendingWorkRequestTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(pendingWorkRequestTable);

        jLabel2.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(162, 162, 162));
        jLabel2.setText("REQUEST PENDING");

        pendingDeleteButton.setBackground(new java.awt.Color(51, 102, 204));
        pendingDeleteButton.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        pendingDeleteButton.setForeground(new java.awt.Color(255, 255, 255));
        pendingDeleteButton.setText("DELETE");
        pendingDeleteButton.setEnabled(false);
        pendingDeleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pendingDeleteButtonActionPerformed(evt);
            }
        });

        acceptPendingButton.setBackground(new java.awt.Color(51, 102, 204));
        acceptPendingButton.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        acceptPendingButton.setForeground(new java.awt.Color(255, 255, 255));
        acceptPendingButton.setText("ACCEPT");
        acceptPendingButton.setEnabled(false);
        acceptPendingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                acceptPendingButtonActionPerformed(evt);
            }
        });

        rejectPendingButton.setBackground(new java.awt.Color(51, 102, 204));
        rejectPendingButton.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        rejectPendingButton.setForeground(new java.awt.Color(255, 255, 255));
        rejectPendingButton.setText("REJECT");
        rejectPendingButton.setEnabled(false);
        rejectPendingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rejectPendingButtonActionPerformed(evt);
            }
        });

        deleteMyButton.setBackground(new java.awt.Color(51, 102, 204));
        deleteMyButton.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        deleteMyButton.setForeground(new java.awt.Color(255, 255, 255));
        deleteMyButton.setText("DELETE");
        deleteMyButton.setEnabled(false);
        deleteMyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteMyButtonActionPerformed(evt);
            }
        });

        rejectMyButton.setBackground(new java.awt.Color(51, 102, 204));
        rejectMyButton.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        rejectMyButton.setForeground(new java.awt.Color(255, 255, 255));
        rejectMyButton.setText("REJECT");
        rejectMyButton.setEnabled(false);
        rejectMyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rejectMyButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(deleteMyButton)
                        .addGap(135, 135, 135)
                        .addComponent(rejectMyButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pendingDeleteButton)
                        .addGap(31, 31, 31)
                        .addComponent(acceptPendingButton)
                        .addGap(31, 31, 31)
                        .addComponent(rejectPendingButton))
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 730, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pendingDeleteButton)
                    .addComponent(acceptPendingButton)
                    .addComponent(rejectPendingButton))
                .addGap(56, 56, 56)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deleteMyButton)
                    .addComponent(rejectMyButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void pendingDeleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pendingDeleteButtonActionPerformed

         for (int i = 0; i < pendingWorkRequestTable.getRowCount(); i++) {
            boolean isChecked = (Boolean) pendingWorkRequestTable.getValueAt(i, 6);

            if (isChecked) {
                WorkRequest workRequest = (WorkRequest)pendingWorkRequestTable.getValueAt(i, 0);
                 enterprise.deleteWorkRequest(workRequest);
            }
        }
        
        populateMyWorkRequestTable();
        populatePendingRequestTable();
    }//GEN-LAST:event_pendingDeleteButtonActionPerformed

   
    private void acceptPendingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_acceptPendingButtonActionPerformed

        for (int i = 0; i < pendingWorkRequestTable.getRowCount(); i++) {
            boolean isChecked = (Boolean) pendingWorkRequestTable.getValueAt(i, 6);

            if (isChecked) {
                VehicleServiceRequest workRequest = (VehicleServiceRequest)pendingWorkRequestTable.getValueAt(i, 0);
                workRequest.setReceiver(vehicle);
                workRequest.setAssignDate(new Date());
                workRequest.setRequestStatus(RequestStatus.REQUEST_ACCEPTED);
                vehicle.getWorkQueue().addWorkRequest(workRequest);
            }
        }
        
        populateMyWorkRequestTable();
        populatePendingRequestTable();
        
    }//GEN-LAST:event_acceptPendingButtonActionPerformed

    private void rejectPendingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rejectPendingButtonActionPerformed

           for (int i = 0; i < pendingWorkRequestTable.getRowCount(); i++) {
            boolean isChecked = (Boolean) pendingWorkRequestTable.getValueAt(i, 6);

            if (isChecked) {
                VehicleServiceRequest workRequest = (VehicleServiceRequest)pendingWorkRequestTable.getValueAt(i, 0);
                workRequest.setRequestStatus(RequestStatus.REQUEST_DENIED);
                vehicle.getWorkQueue().addWorkRequest(workRequest);
            }
        }
        
        populateMyWorkRequestTable();
        populatePendingRequestTable();
    }//GEN-LAST:event_rejectPendingButtonActionPerformed

    private void deleteMyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteMyButtonActionPerformed
          for (int i = 0; i < workRequestTable.getRowCount(); i++) {
            boolean isChecked = (Boolean) workRequestTable.getValueAt(i, 7);

            if (isChecked) {
                WorkRequest workRequest = (WorkRequest)workRequestTable.getValueAt(i, 0);
                 enterprise.deleteWorkRequest(workRequest);
            }
        }
        
        populateMyWorkRequestTable();
        populatePendingRequestTable();
    }//GEN-LAST:event_deleteMyButtonActionPerformed

    private void rejectMyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rejectMyButtonActionPerformed

        
           for (int i = 0; i < pendingWorkRequestTable.getRowCount(); i++) {
            boolean isChecked = (Boolean) pendingWorkRequestTable.getValueAt(i, 7);

            if (isChecked) {
                VehicleServiceRequest workRequest = (VehicleServiceRequest)pendingWorkRequestTable.getValueAt(i, 0);
                workRequest.setRequestStatus(RequestStatus.REQUEST_DENIED);             
            }
        }
        
        populateMyWorkRequestTable();
        populatePendingRequestTable();
        
    }//GEN-LAST:event_rejectMyButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton acceptPendingButton;
    private javax.swing.JButton deleteMyButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton pendingDeleteButton;
    private javax.swing.JTable pendingWorkRequestTable;
    private javax.swing.JButton rejectMyButton;
    private javax.swing.JButton rejectPendingButton;
    private javax.swing.JTable workRequestTable;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onNotificationState(BroadcastMessage a_NotificationState, Map<BroadcastStateKey, Object> a_Parameters) {
        if(a_NotificationState != null && a_NotificationState == BroadcastMessage.BROADCAST){
            if(a_Parameters != null && a_Parameters.containsKey(BroadcastStateKey.BROADCAST_MESSAGE) 
                    && (a_Parameters.get(BroadcastStateKey.BROADCAST_MESSAGE) instanceof BroadcastValue)){
                BroadcastValue broadcastValue = (BroadcastValue)a_Parameters.get(BroadcastStateKey.BROADCAST_MESSAGE);
                if(broadcastValue == BroadcastValue.VEHICLE_CREATED_APPOINTMENT || broadcastValue == BroadcastValue.VEHICLE_STATUS_UPDATED){
                    populateMyWorkRequestTable();
                    populatePendingRequestTable();
                }
            }
        }
    }
}
