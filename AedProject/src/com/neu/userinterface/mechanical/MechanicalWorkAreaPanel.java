/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.userinterface.mechanical;

import com.neu.business.ConfigureASystem;
import com.neu.business.IManagerDelegate;
import com.neu.business.enterprise.Enterprise;
import com.neu.business.manager.BroadcastMessage;
import com.neu.business.manager.BroadcastStateKey;
import com.neu.business.manager.BroadcastValue;
import com.neu.business.manager.IBroadcastObserver;
import com.neu.business.useraccount.UserAccount;
import com.neu.utils.Toast;
import com.neu.business.vehicle.Vehicle;
import com.neu.business.vehicle.VehicleCondition;
import com.neu.business.vehicle.VehiclePart;
import com.neu.business.vehicle.VehiclePartCondition;
import com.neu.business.workqueue.RequestStatus;
import com.neu.business.workqueue.VehicleServiceRequest;
import com.neu.business.workqueue.WorkRequest;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sabrish
 */
public class MechanicalWorkAreaPanel extends javax.swing.JPanel implements IBroadcastObserver {

    /**
     * Creates new form MechanicalWorkAreaPanel
     */
    private JPanel userProcessContainer;
    private Enterprise enterprise;
    private UserAccount userAccount;

    public MechanicalWorkAreaPanel(JPanel userProcessContainer, Enterprise enterprise, UserAccount userAccount) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        
        updateView();
        IManagerDelegate managerDelegate = ConfigureASystem.getInstance().getManagerDelegate();
        managerDelegate.registerBroadcastObserver(this);
        addContainerListener(new ContainerAdapter() {

            @Override
            public void componentRemoved(ContainerEvent e) {
                managerDelegate.unregisterBroadcastObserver(MechanicalWorkAreaPanel.this);
            }

        });
    }

    private void updateView() {
        requestTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    onRowSelectionChanged();
                }
            }
        });
        populateRequestTable();
    }

    private void onRowSelectionChanged() {
        int selectedItem = requestTable.getSelectedRow();

        if (selectedItem > -1) {

            VehicleServiceRequest workRequest = (VehicleServiceRequest) requestTable.getValueAt(selectedItem, 0);
            populateConditionTable();
            updateViews(workRequest);

        }
    }

    private void populateStatusComboBox() {

        statusComboBox.removeAllItems();
        for (RequestStatus requestStatus : RequestStatus.values()) {
            if (requestStatus != RequestStatus.REQUEST_TRANSFFERED) {
                statusComboBox.addItem(requestStatus);
            }
        }

    }

    private void updateViews(VehicleServiceRequest vehicleServiceRequest) {

        requestLabel.setText(vehicleServiceRequest.getVehicleRequestType().getValue());
        for (int i = 0; i < statusComboBox.getItemCount(); i++) {
            if (statusComboBox.getItemAt(i) == vehicleServiceRequest.getRequestStatus()) {
                statusComboBox.setSelectedIndex(i);
                break;
            }
        }
        senderLabel.setText(vehicleServiceRequest.getSender().getPerson().getName());
        receiverLabel1.setText(vehicleServiceRequest.getReceiver().getPerson().getName());
        mesageTextArea1.setText(vehicleServiceRequest.getMessage());
        populateStatusComboBox();
    }

    private void populateConditionTable() {
        int selectedItem = requestTable.getSelectedRow();

        if (selectedItem > -1) {

            Vehicle vehicle = null;

            WorkRequest workRequest = (WorkRequest) requestTable.getValueAt(selectedItem, 0);
            if (workRequest != null) {
                if (workRequest.getSender() != null && workRequest.getSender().getPerson() != null && (workRequest.getSender().getPerson() instanceof Vehicle)) {
                    vehicle = (Vehicle) workRequest.getSender().getPerson();
                } else if (workRequest.getReceiver() != null && workRequest.getReceiver().getPerson() != null && (workRequest.getReceiver().getPerson() instanceof Vehicle)) {
                    vehicle = (Vehicle) workRequest.getSender().getPerson();
                }

                if (vehicle != null) {
                    Map<VehiclePart, VehiclePartCondition> vehiclePartConditions = vehicle.getVehiclePartConditions();
                    if (vehiclePartConditions != null && vehiclePartConditions.size() > 0) {
                        DefaultTableModel defaultTableModel = (DefaultTableModel) conditionTable.getModel();
                        defaultTableModel.setRowCount(0);
                        for (Map.Entry<VehiclePart, VehiclePartCondition> entrySet : vehiclePartConditions.entrySet()) {
                            Object[] row = new Object[2];
                            row[0] = entrySet.getValue();
                            row[1] = entrySet.getValue().getCondition();
                            defaultTableModel.addRow(row);
                        }
                    }
                }
            }
        }
    }

    private void visibleUI(boolean visible) {
        conditionTitleLabel.setVisible(visible);
        conditionTabelPanel.setVisible(visible);
        requestLabel.setVisible(visible);
        requestTitle.setVisible(visible);
        conditionTable.setVisible(visible);
        updateStatusLabel.setVisible(visible);
        statusTitle.setVisible(visible);
        statusComboBox.setVisible(visible);
        senderLabel.setVisible(visible);
        senderTitle.setVisible(visible);
        receiverLabel1.setVisible(visible);
        receiverTitle.setVisible(visible);
        mesageTextArea1.setVisible(visible);
        messageTitle.setVisible(visible);
        updateStatusButton.setVisible(visible);
        resolvedLabel.setVisible(visible);
        engineCheckBox.setVisible(visible);
        carboratorCheckBox.setVisible(visible);
        acCheckBox.setVisible(visible);
        brakeCheckBox.setVisible(visible);
        pcCheckBox.setVisible(visible);
    }

    private void populateRequestTable() {
        if (enterprise != null) {
            if (userAccount != null) {
                ArrayList<WorkRequest> workRequests = userAccount.getWorkQueue().getWorkRequestList();
                if (workRequests != null && workRequests.size() > 0) {
                    visibleUI(true);
                    DefaultTableModel defaultTableModel = (DefaultTableModel) requestTable.getModel();
                    defaultTableModel.setRowCount(0);
                    for (WorkRequest workRequest : workRequests) {
                        VehicleServiceRequest vehicleServiceRequest = (VehicleServiceRequest) workRequest;
                        Object[] row = new Object[2];
                        row[0] = vehicleServiceRequest;
                        row[1] = vehicleServiceRequest.getRequestStatus();
                        defaultTableModel.addRow(row);
                    }
                }

                if (workRequests.size() > 0) {
                    requestTable.setRowSelectionInterval(0, 0);
                }

                if (workRequests.isEmpty()) {
                    visibleUI(false);
                }
            } else {
                visibleUI(false);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        requestTable = new javax.swing.JTable();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        conditionTitleLabel = new javax.swing.JLabel();
        updateStatusLabel = new javax.swing.JLabel();
        requestTitle = new javax.swing.JLabel();
        requestLabel = new javax.swing.JLabel();
        statusTitle = new javax.swing.JLabel();
        statusComboBox = new javax.swing.JComboBox();
        senderTitle = new javax.swing.JLabel();
        senderLabel = new javax.swing.JLabel();
        receiverTitle = new javax.swing.JLabel();
        messageTitle = new javax.swing.JLabel();
        workRequestLabel = new javax.swing.JLabel();
        receiverLabel1 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        mesageTextArea1 = new javax.swing.JTextArea();
        conditionTabelPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        conditionTable = new javax.swing.JTable();
        updateStatusButton = new javax.swing.JButton();
        resolvedLabel = new javax.swing.JLabel();
        engineCheckBox = new javax.swing.JCheckBox();
        carboratorCheckBox = new javax.swing.JCheckBox();
        brakeCheckBox = new javax.swing.JCheckBox();
        acCheckBox = new javax.swing.JCheckBox();
        pcCheckBox = new javax.swing.JCheckBox();

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(162, 162, 162));
        jLabel1.setText("WORK REQUEST");

        requestTable.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        requestTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "REQUEST", "STATUS"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        requestTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(requestTable);
        if (requestTable.getColumnModel().getColumnCount() > 0) {
            requestTable.getColumnModel().getColumn(0).setResizable(false);
        }

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setPreferredSize(new java.awt.Dimension(10, 50));

        conditionTitleLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        conditionTitleLabel.setForeground(new java.awt.Color(162, 162, 162));
        conditionTitleLabel.setText("CONDITION");

        updateStatusLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        updateStatusLabel.setForeground(new java.awt.Color(162, 162, 162));
        updateStatusLabel.setText("UPDATE STATUS");

        requestTitle.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        requestTitle.setForeground(new java.awt.Color(162, 162, 162));
        requestTitle.setText("REQUEST");

        requestLabel.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        requestLabel.setForeground(new java.awt.Color(162, 162, 162));
        requestLabel.setText("request");

        statusTitle.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        statusTitle.setForeground(new java.awt.Color(162, 162, 162));
        statusTitle.setText("STATUS");

        statusComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        senderTitle.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        senderTitle.setForeground(new java.awt.Color(162, 162, 162));
        senderTitle.setText("SENDER");

        senderLabel.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        senderLabel.setForeground(new java.awt.Color(162, 162, 162));
        senderLabel.setText("sender1");

        receiverTitle.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        receiverTitle.setForeground(new java.awt.Color(162, 162, 162));
        receiverTitle.setText("RECEIVER");

        messageTitle.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        messageTitle.setForeground(new java.awt.Color(162, 162, 162));
        messageTitle.setText("MESSAGE");

        workRequestLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        workRequestLabel.setForeground(new java.awt.Color(162, 162, 162));
        workRequestLabel.setText("WORK REQUEST TABLE");

        receiverLabel1.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        receiverLabel1.setForeground(new java.awt.Color(162, 162, 162));
        receiverLabel1.setText("receiver");

        mesageTextArea1.setColumns(20);
        mesageTextArea1.setRows(5);
        jScrollPane4.setViewportView(mesageTextArea1);

        conditionTable.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        conditionTable.setForeground(new java.awt.Color(162, 162, 162));
        conditionTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "PART", "CONDITION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(conditionTable);

        javax.swing.GroupLayout conditionTabelPanelLayout = new javax.swing.GroupLayout(conditionTabelPanel);
        conditionTabelPanel.setLayout(conditionTabelPanelLayout);
        conditionTabelPanelLayout.setHorizontalGroup(
            conditionTabelPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
        );
        conditionTabelPanelLayout.setVerticalGroup(
            conditionTabelPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
        );

        updateStatusButton.setBackground(new java.awt.Color(53, 111, 255));
        updateStatusButton.setForeground(new java.awt.Color(255, 255, 255));
        updateStatusButton.setText("UPDATE STATUS");
        updateStatusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateStatusButtonActionPerformed(evt);
            }
        });

        resolvedLabel.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        resolvedLabel.setForeground(new java.awt.Color(162, 162, 162));
        resolvedLabel.setText("RESOLVED");

        engineCheckBox.setBackground(new java.awt.Color(255, 255, 255));
        engineCheckBox.setText("Engine");

        carboratorCheckBox.setBackground(new java.awt.Color(255, 255, 255));
        carboratorCheckBox.setText("Carborator");

        brakeCheckBox.setBackground(new java.awt.Color(255, 255, 255));
        brakeCheckBox.setText("Brake");

        acCheckBox.setBackground(new java.awt.Color(255, 255, 255));
        acCheckBox.setText("AC");

        pcCheckBox.setBackground(new java.awt.Color(255, 255, 255));
        pcCheckBox.setText("Pollution Control");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(workRequestLabel))
                        .addGap(18, 18, 18)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(conditionTitleLabel)
                            .addComponent(updateStatusLabel)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(requestTitle)
                                .addGap(79, 79, 79)
                                .addComponent(requestLabel))
                            .addComponent(conditionTabelPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(updateStatusButton)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(statusTitle)
                                    .addComponent(senderTitle)
                                    .addComponent(receiverTitle)
                                    .addComponent(messageTitle)
                                    .addComponent(resolvedLabel))
                                .addGap(71, 71, 71)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane4)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(receiverLabel1)
                                            .addComponent(senderLabel)
                                            .addComponent(statusComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(engineCheckBox)
                                                    .addComponent(acCheckBox))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(pcCheckBox)
                                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                                        .addComponent(carboratorCheckBox)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(brakeCheckBox)))))
                                        .addGap(0, 0, 0))))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(326, 326, 326)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jSeparator3, javax.swing.GroupLayout.DEFAULT_SIZE, 775, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(workRequestLabel)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel1)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 567, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(conditionTitleLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(conditionTabelPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(updateStatusLabel)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(requestTitle)
                                    .addComponent(requestLabel))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(statusTitle)
                                    .addComponent(statusComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(senderLabel)
                                    .addComponent(senderTitle))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(receiverTitle)
                                    .addComponent(receiverLabel1))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(messageTitle)
                                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(23, 23, 23)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(resolvedLabel)
                                    .addComponent(engineCheckBox)
                                    .addComponent(carboratorCheckBox)
                                    .addComponent(brakeCheckBox))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(acCheckBox)
                                    .addComponent(pcCheckBox))
                                .addGap(7, 7, 7)
                                .addComponent(updateStatusButton)))))
                .addGap(0, 0, 0))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(563, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 583, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void updateStatusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateStatusButtonActionPerformed
        int selectedItem = requestTable.getSelectedRow();

        if (selectedItem > -1) {

            VehicleServiceRequest vehicleServiceRequest = (VehicleServiceRequest) requestTable.getValueAt(selectedItem, 0);
            if (vehicleServiceRequest != null) {
                RequestStatus requestStatus = (RequestStatus) statusComboBox.getSelectedItem();
                vehicleServiceRequest.setRequestStatus(requestStatus);
                vehicleServiceRequest.setMessage(mesageTextArea1.getText());
                if ((requestStatus == RequestStatus.REQUEST_CLOSED) || ((requestStatus == RequestStatus.REQUEST_DENIED))) {
                    vehicleServiceRequest.setResolveDate(new Date());
                } else if ((requestStatus == RequestStatus.REQUEST_OPEN) || (requestStatus == RequestStatus.REQUEST_PENDING)) {
                    vehicleServiceRequest.setResolveDate(null);
                } else if ((requestStatus == RequestStatus.REQUEST_ACCEPTED)) {
                    if (vehicleServiceRequest.getSender() != null && vehicleServiceRequest.getSender().getPerson() != null
                            && !(vehicleServiceRequest.getSender().getPerson() instanceof Vehicle)) {
                        vehicleServiceRequest.setSender(userAccount);

                    } else if (vehicleServiceRequest.getReceiver() != null && vehicleServiceRequest.getReceiver().getPerson() != null
                            && !(vehicleServiceRequest.getReceiver().getPerson() instanceof Vehicle)) {
                        vehicleServiceRequest.setReceiver(userAccount);
                    }
                    userAccount.getWorkQueue().addWorkRequest(vehicleServiceRequest);
                }
                Toast.makeText(null, "Request updated successfully", Toast.LENGTH_SHORT, Toast.Style.SUCCESS).display();
                updateResolvedStatus(vehicleServiceRequest);
                Map<BroadcastStateKey, Object> map = new HashMap<>();
                map.put(BroadcastStateKey.BROADCAST_MESSAGE, BroadcastValue.VEHICLE_REQUEST_UPDATED);
                ConfigureASystem.getInstance().getManagerDelegate().sendBroadCast(map);
            } else {
                Toast.makeText(null, "Error in updating request,Please try again ", Toast.LENGTH_SHORT, Toast.Style.ERROR).display();
            }
        }
    }//GEN-LAST:event_updateStatusButtonActionPerformed

    private void updateResolvedStatus(VehicleServiceRequest vehicleServiceRequest) {

        if (vehicleServiceRequest != null) {
            Vehicle vehicle = null;
            if (vehicleServiceRequest.getSender() != null && vehicleServiceRequest.getSender().getPerson() != null
                    && (vehicleServiceRequest.getSender().getPerson() instanceof Vehicle)) {
                vehicle = (Vehicle) vehicleServiceRequest.getSender().getPerson();

            } else if (vehicleServiceRequest.getReceiver() != null && vehicleServiceRequest.getReceiver().getPerson() != null
                    && (vehicleServiceRequest.getReceiver().getPerson() instanceof Vehicle)) {
                vehicle = (Vehicle) vehicleServiceRequest.getReceiver().getPerson();
            }
            if (engineCheckBox.isSelected()) {

                vehicleServiceRequest.addResolvedVehiclePartCondition(VehiclePart.ENGINE, VehicleCondition.EXCELLENT);
                if(vehicle != null)
                vehicle.addResolvedVehiclePartCondition(VehiclePart.ENGINE, VehicleCondition.EXCELLENT);
            }

            if (carboratorCheckBox.isSelected()) {
                vehicleServiceRequest.addResolvedVehiclePartCondition(VehiclePart.CARBORATOR, VehicleCondition.EXCELLENT);
                 if(vehicle != null)
                vehicle.addResolvedVehiclePartCondition(VehiclePart.CARBORATOR, VehicleCondition.EXCELLENT);
            }

            if (brakeCheckBox.isSelected()) {
                vehicleServiceRequest.addResolvedVehiclePartCondition(VehiclePart.BRAKE, VehicleCondition.EXCELLENT);
                if(vehicle != null)
                vehicle.addResolvedVehiclePartCondition(VehiclePart.BRAKE, VehicleCondition.EXCELLENT);
            }

            if (acCheckBox.isSelected()) {
                vehicleServiceRequest.addResolvedVehiclePartCondition(VehiclePart.AC, VehicleCondition.EXCELLENT);
                if(vehicle != null)
                vehicle.addResolvedVehiclePartCondition(VehiclePart.AC, VehicleCondition.EXCELLENT);
            }

            if (pcCheckBox.isSelected()) {
                vehicleServiceRequest.addResolvedVehiclePartCondition(VehiclePart.POLLUTION_CONTROL, VehicleCondition.EXCELLENT);
                if(vehicle != null)
                vehicle.addResolvedVehiclePartCondition(VehiclePart.POLLUTION_CONTROL, VehicleCondition.EXCELLENT);
            }
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox acCheckBox;
    private javax.swing.JCheckBox brakeCheckBox;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox carboratorCheckBox;
    private javax.swing.JPanel conditionTabelPanel;
    private javax.swing.JTable conditionTable;
    private javax.swing.JLabel conditionTitleLabel;
    private javax.swing.JCheckBox engineCheckBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTextArea mesageTextArea1;
    private javax.swing.JLabel messageTitle;
    private javax.swing.JCheckBox pcCheckBox;
    private javax.swing.JLabel receiverLabel1;
    private javax.swing.JLabel receiverTitle;
    private javax.swing.JLabel requestLabel;
    private javax.swing.JTable requestTable;
    private javax.swing.JLabel requestTitle;
    private javax.swing.JLabel resolvedLabel;
    private javax.swing.JLabel senderLabel;
    private javax.swing.JLabel senderTitle;
    private javax.swing.JComboBox statusComboBox;
    private javax.swing.JLabel statusTitle;
    private javax.swing.JButton updateStatusButton;
    private javax.swing.JLabel updateStatusLabel;
    private javax.swing.JLabel workRequestLabel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onNotificationState(BroadcastMessage a_NotificationState, Map<BroadcastStateKey, Object> a_Parameters) {
        if (a_NotificationState != null && a_NotificationState == BroadcastMessage.BROADCAST) {
            if (a_Parameters != null && a_Parameters.containsKey(BroadcastStateKey.BROADCAST_MESSAGE) && (a_Parameters.get(BroadcastStateKey.BROADCAST_MESSAGE) instanceof BroadcastValue)){
                BroadcastValue broadcastValue = (BroadcastValue)a_Parameters.get(BroadcastStateKey.BROADCAST_MESSAGE);
                if ((broadcastValue == BroadcastValue.VEHICLE_REQUEST_ACCEPTED) || (broadcastValue == BroadcastValue.VEHICLE_REQUEST_UPDATED)) {
                    populateRequestTable();
                }
            }
        }
    }
}
