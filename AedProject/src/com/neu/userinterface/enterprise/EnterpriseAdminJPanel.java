/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.userinterface.enterprise;

import com.neu.business.ErrorCode;
import com.neu.business.Response;
import com.neu.business.employee.Employee;
import com.neu.business.enterprise.Enterprise;
import com.neu.business.organization.EmployeeOrganisation;
import com.neu.business.organization.Organization;
import com.neu.business.organization.VehicleOrganization;
import com.neu.business.useraccount.UserAccount;
import com.neu.userinterface.LoginJPanel;
import com.neu.userinterface.vehicle.VehicleUserSignupJpanel;
import com.neu.utils.Toast;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sabrish
 */
public class EnterpriseAdminJPanel extends javax.swing.JPanel {

    /**
     * Creates new form EnterpriseAdminJPanel
     */
    private Enterprise enterprise;
    private JPanel userProcessContainer;

    public EnterpriseAdminJPanel(JPanel userProcessContainer, Enterprise enterprise) {
        initComponents();
        this.enterprise = enterprise;
        this.userProcessContainer = userProcessContainer;
        errorLabel.setVisible(false);
        updateView();
    }

    private void updateView() {

        organisationTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    onRowSelectionChanged();
                }
            }
        });

        enterpriseName.setText(enterprise.getName());
        populateOrganizationComboBox();
        populateOrganisation();
    }

    public void populateOrganizationComboBox() {
        organizationEmpJComboBox.removeAllItems();
        if (enterprise.getEnterpriseType() == Enterprise.EnterpriseType.LOCATION_CENTRE) {
            for (Organization.Type type : Organization.Type.values()) {
                if (!(type.getValue().equals(Organization.Type.AdminOrganization.getValue()) || type.getValue().equals(Organization.Type.AnalysisOrganisation.getValue()))) {
                    organizationEmpJComboBox.addItem(type);
                }
            }
        }
        else if (enterprise.getEnterpriseType() == Enterprise.EnterpriseType.ANALYSIS) {
            organizationEmpJComboBox.addItem(Organization.Type.AnalysisOrganisation);
        }
    }

    private void onRowSelectionChanged() {
        int selectedItem = organisationTable.getSelectedRow();

        if (selectedItem > -1) {

            Organization organization = (Organization) organisationTable.getValueAt(selectedItem, 0);
            if (organization != null) {
                organisationNameLabel.setText(organization.getName());
            }

        }
        populateEmployeeList();
    }

    public void populateEmployeeList() {
        int selectedItem = organisationTable.getSelectedRow();
        if (selectedItem > -1) {

            Organization organization = (Organization) organisationTable.getValueAt(selectedItem, 0);
            if (organization != null) {

                ArrayList<UserAccount> employees = organization.getUserAccountManager().getUserAccountList();
                if (employees != null) {
                    DefaultTableModel defaultTableModel = (DefaultTableModel) personTable.getModel();
                    defaultTableModel.setRowCount(0);
                    for (UserAccount employee : employees) {
                        Object[] row = new Object[2];
                        row[0] = employee;
                        row[1] = employee.getPerson().getName();
                        defaultTableModel.addRow(row);
                    }
                }
            }
        }
    }

    public void populateOrganisation() {

        if (enterprise != null) {

            ArrayList<Organization> organizations = enterprise.getOrganizationDirectory().getOrganizationList();
            if (organizations != null && organizations.size() >= 0) {
                visibleUI(true);
                DefaultTableModel defaultTableModel = (DefaultTableModel) organisationTable.getModel();
                defaultTableModel.setRowCount(0);
                for (Organization organization : organizations) {
                    Object[] row = new Object[1];
                    row[0] = organization;
                    defaultTableModel.addRow(row);
                }
            if(organizations.size() > 0) {
                organisationTable.setRowSelectionInterval(0, 0);
               }
           
           if(organizations.isEmpty())
               visibleUI(false);
               
            }else{
                visibleUI(false);
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        addButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        organisationTable = new javax.swing.JTable();
        enterpriseName = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        organizationEmpJComboBox = new javax.swing.JComboBox();
        organisationNameLabel = new javax.swing.JLabel();
        userDirectoryLabel = new javax.swing.JLabel();
        addPersonButton = new javax.swing.JButton();
        deletePersonButton = new javax.swing.JButton();
        logoutButton = new javax.swing.JButton();
        errorLabel = new javax.swing.JLabel();
        tablePanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        personTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(23, 111, 152));
        setLayout(new java.awt.GridBagLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(162, 162, 162));
        jLabel4.setText("ADD ORGANIZATION");

        addButton.setBackground(new java.awt.Color(53, 153, 255));
        addButton.setForeground(new java.awt.Color(255, 255, 255));
        addButton.setText("+");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        organisationTable.setForeground(new java.awt.Color(162, 162, 162));
        organisationTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Organisation"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        organisationTable.setSelectionBackground(new java.awt.Color(255, 255, 255));
        organisationTable.setSelectionForeground(new java.awt.Color(162, 162, 162));
        jScrollPane1.setViewportView(organisationTable);

        enterpriseName.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        enterpriseName.setForeground(new java.awt.Color(162, 162, 162));
        enterpriseName.setText("ENTERPRISE NAME");

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setPreferredSize(new java.awt.Dimension(10, 50));

        organisationNameLabel.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        organisationNameLabel.setForeground(new java.awt.Color(162, 162, 162));
        organisationNameLabel.setText(" ORGANISATION NAME");

        userDirectoryLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        userDirectoryLabel.setForeground(new java.awt.Color(162, 162, 162));
        userDirectoryLabel.setText("USER DIRECTORY");

        addPersonButton.setBackground(new java.awt.Color(53, 153, 255));
        addPersonButton.setForeground(new java.awt.Color(255, 255, 255));
        addPersonButton.setText("ADD USER");
        addPersonButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addPersonButtonActionPerformed(evt);
            }
        });

        deletePersonButton.setBackground(new java.awt.Color(53, 153, 255));
        deletePersonButton.setForeground(new java.awt.Color(255, 255, 255));
        deletePersonButton.setText("DELETE USER");
        deletePersonButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletePersonButtonActionPerformed(evt);
            }
        });

        logoutButton.setBackground(new java.awt.Color(51, 153, 255));
        logoutButton.setForeground(new java.awt.Color(255, 255, 255));
        logoutButton.setText("LOGOUT");
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutButtonActionPerformed(evt);
            }
        });

        errorLabel.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        errorLabel.setForeground(new java.awt.Color(255, 0, 0));
        errorLabel.setText("Error");

        personTable.setForeground(new java.awt.Color(162, 162, 162));
        personTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "USERNAME", "NAME"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        personTable.setSelectionBackground(new java.awt.Color(255, 255, 255));
        personTable.setSelectionForeground(new java.awt.Color(204, 204, 204));
        jScrollPane2.setViewportView(personTable);
        if (personTable.getColumnModel().getColumnCount() > 0) {
            personTable.getColumnModel().getColumn(0).setResizable(false);
            personTable.getColumnModel().getColumn(1).setResizable(false);
        }

        javax.swing.GroupLayout tablePanelLayout = new javax.swing.GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        tablePanelLayout.setVerticalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 760, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(organizationEmpJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(26, 26, 26)
                                        .addComponent(addButton))
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(organisationNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(48, 48, 48)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(userDirectoryLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(tablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(addPersonButton)
                                                .addGap(34, 34, 34)
                                                .addComponent(deletePersonButton))))))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(363, 363, 363)
                        .addComponent(enterpriseName)
                        .addGap(190, 190, 190)
                        .addComponent(logoutButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(errorLabel)))
                .addGap(30, 30, 30))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 466, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(enterpriseName)
                            .addComponent(logoutButton))
                        .addGap(6, 6, 6)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jLabel4)
                                .addGap(12, 12, 12)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(organizationEmpJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(addButton))
                                .addGap(27, 27, 27)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(organisationNameLabel)
                                .addGap(38, 38, 38)
                                .addComponent(userDirectoryLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(addPersonButton)
                                    .addComponent(deletePersonButton))))))
                .addGap(18, 18, 18)
                .addComponent(errorLabel)
                .addContainerGap())
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipady = 28;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 100, 18, 44);
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed

        Organization.Type type = (Organization.Type)organizationEmpJComboBox.getSelectedItem();
        Response<Organization> response = enterprise.getOrganizationDirectory().createOrganization(type);
        if(response.getErrorCode() == ErrorCode.SUCCESS){
             populateOrganisation();
             errorLabel.setText("");
             errorLabel.setVisible(false);
               Toast.makeText(null, "Organisation created successfully", Toast.LENGTH_SHORT, Toast.Style.SUCCESS).display();
        }else if(response.getErrorCode() == ErrorCode.ALREADY_EXIST){
            showError(type.getValue()+" already exist.");
        }else{
            showError("Something went wrong. Please try again");
        }
            
       
    }//GEN-LAST:event_addButtonActionPerformed

    private void addPersonButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addPersonButtonActionPerformed

        int selectedItem = organisationTable.getSelectedRow();
        Organization organization = (Organization) organisationTable.getValueAt(selectedItem, 0);
        if (organization != null) {
            organisationNameLabel.setText(organization.getName());
            if(organization instanceof EmployeeOrganisation){
            CreateEmployeePanel mainWorkAreaJPanel = new CreateEmployeePanel(userProcessContainer, organization, enterprise);
            userProcessContainer.add("CreateEmployeePanel", mainWorkAreaJPanel);
            CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
            cardLayout.next(userProcessContainer);
            }else if(organization instanceof VehicleOrganization){
                VehicleUserSignupJpanel vehicleUserSignupJpanel = new VehicleUserSignupJpanel(userProcessContainer, enterprise);
                userProcessContainer.add("VehicleUserSignupJpanel", vehicleUserSignupJpanel);
                CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
                cardLayout.next(userProcessContainer);
            }
        }else{
            Toast.makeText(null, "Please select a Organisation", Toast.LENGTH_SHORT, Toast.Style.ERROR).display();
        }

    }//GEN-LAST:event_addPersonButtonActionPerformed

    private void deletePersonButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletePersonButtonActionPerformed

        int selectedItem = organisationTable.getSelectedRow();
        if (selectedItem > -1) {
            Organization organization = (Organization) organisationTable.getValueAt(selectedItem, 0);
            if (organization != null) {
                int personSelectedItem = personTable.getSelectedRow();
                if (personSelectedItem > -1) {
                    UserAccount userAccount = (UserAccount) personTable.getValueAt(selectedItem, 0);
                    if(organization instanceof EmployeeOrganisation){
                    organization.getEmployeeManager().removeEmployee((Employee) userAccount.getPerson());
                    }
                    organization.getUserAccountManager().removeUserAccount(userAccount);
                    Toast.makeText(new JFrame(), "User deleted successfully", Toast.LENGTH_SHORT, Toast.Style.SUCCESS).display();
                    populateEmployeeList();
                }else {
                Toast.makeText(new JFrame(), "Please select a row", Toast.LENGTH_SHORT, Toast.Style.ERROR).display();
                }
            }

        } else {
                Toast.makeText(new JFrame(), "Please select a Organisation", Toast.LENGTH_SHORT, Toast.Style.ERROR).display();
        }

    }//GEN-LAST:event_deletePersonButtonActionPerformed

    private void showError(String a_text){
        errorLabel.setText(a_text);
        errorLabel.setVisible(true);
    }
    
    private void logoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutButtonActionPerformed
        userProcessContainer.removeAll();
        LoginJPanel loginJPanel = new LoginJPanel(userProcessContainer);
        userProcessContainer.add("LoginJPanel", loginJPanel);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.next(userProcessContainer);
    }//GEN-LAST:event_logoutButtonActionPerformed

    private void visibleUI(boolean visible){
        
        organisationNameLabel.setVisible(visible);
        userDirectoryLabel.setVisible(visible);
        tablePanel.setVisible(visible);
        addPersonButton.setVisible(visible);
        deletePersonButton.setVisible(visible);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JButton addPersonButton;
    private javax.swing.JButton deletePersonButton;
    private javax.swing.JLabel enterpriseName;
    private javax.swing.JLabel errorLabel;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JButton logoutButton;
    private javax.swing.JLabel organisationNameLabel;
    private javax.swing.JTable organisationTable;
    private javax.swing.JComboBox organizationEmpJComboBox;
    private javax.swing.JTable personTable;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JLabel userDirectoryLabel;
    // End of variables declaration//GEN-END:variables
}
