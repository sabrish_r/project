/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.userinterface.service;

import com.neu.business.ConfigureASystem;
import com.neu.business.ErrorCode;
import com.neu.business.IManagerDelegate;
import com.neu.business.Response;
import com.neu.business.enterprise.Enterprise;
import com.neu.business.manager.BroadcastMessage;
import com.neu.business.manager.BroadcastStateKey;
import com.neu.business.manager.BroadcastValue;
import com.neu.business.manager.IBroadcastObserver;
import com.neu.business.organization.Organization;
import com.neu.business.organization.ServiceCentreOrganization;
import com.neu.business.organization.VehicleOrganization;
import com.neu.business.useraccount.UserAccount;
import com.neu.utils.Toast;
import com.neu.utils.Validation;
import com.neu.business.vehicle.Vehicle;
import com.neu.business.vehicle.VehiclePart;
import com.neu.business.vehicle.VehiclePartCondition;
import com.neu.business.workqueue.RequestStatus;
import com.neu.business.workqueue.VehicleRequestType;
import com.neu.business.workqueue.VehicleServiceRequest;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sabrish
 */
public class ServiceVehicleUserPanel extends javax.swing.JPanel implements IBroadcastObserver{

    /**
     * Creates new form ServiceVehicleUserPanel
     */
    private JPanel userProcessContainer;
    private Enterprise enterprise;
    private UserAccount userAccount;

    public ServiceVehicleUserPanel(JPanel userProcessContainer, Enterprise enterprise, UserAccount userAccount) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        errorLabel1.setVisible(false);
        populateUserTable();
        updateConditionTableListener();
        IManagerDelegate managerDelegate = ConfigureASystem.getInstance().getManagerDelegate();
        managerDelegate.registerBroadcastObserver(this);
        addContainerListener(new ContainerAdapter() {

            @Override
            public void componentRemoved(ContainerEvent e) {
                managerDelegate.unregisterBroadcastObserver(ServiceVehicleUserPanel.this);
            }
                
                
            });
    }

    private void updateConditionTableListener() {

        userTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    onRowSelectionChanged();
                }
            }
        });

    }

    private void onRowSelectionChanged() {
        int selectedItem = userTable.getSelectedRow();

        if (selectedItem > -1) {

            UserAccount userAccount = (UserAccount) userTable.getValueAt(selectedItem, 0);
            if (userAccount != null) {
                Vehicle vehicle = (Vehicle) userAccount.getPerson();
                upcomingLabel.setText(vehicle.getUpcomingAppointment() == null ? "No Upcoming Appointment" : vehicle.getUpcomingAppointment().toString());
                populateConditionTable();
            }

        }

    }

    private void populateConditionTable() {
        int selectedItem = userTable.getSelectedRow();

        if (selectedItem > -1) {

            UserAccount userAccount = (UserAccount) userTable.getValueAt(selectedItem, 0);
            if (userAccount != null) {
                Vehicle vehicle = (Vehicle) userAccount.getPerson();
                if (vehicle != null) {
                     Map<VehiclePart,VehiclePartCondition> vehiclePartConditions = vehicle.getVehiclePartConditions();
                    if (vehiclePartConditions != null && vehiclePartConditions.size() > 0) {
                        DefaultTableModel defaultTableModel = (DefaultTableModel) conditionTable.getModel();
                        defaultTableModel.setRowCount(0);
                        for (Map.Entry<VehiclePart, VehiclePartCondition> entrySet: vehiclePartConditions.entrySet()) {
                            Object[] row = new Object[2];
                            row[0] = entrySet.getValue();
                            row[1] = entrySet.getValue().getCondition();
                            defaultTableModel.addRow(row);
                        }
                    }
                }
            }
        }
    }

    private void populateUserTable() {

        if (enterprise != null && enterprise.getOrganizationDirectory() != null && enterprise.getOrganizationDirectory().getOrganizationList() != null) {

            VehicleOrganization vehicleOrganization = null;
            for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                if (organization instanceof VehicleOrganization) {
                    vehicleOrganization = (VehicleOrganization) organization;
                    break;
                }
            }
            if (vehicleOrganization != null) {
                visibleUI(true);
                DefaultTableModel defaultTableModel = (DefaultTableModel) userTable.getModel();
                defaultTableModel.setRowCount(0);
                for (UserAccount userAccount : vehicleOrganization.getUserAccountManager().getUserAccountList()) {
                    if (userAccount != null) {

                        Object[] row = new Object[1];
                        row[0] = userAccount;
//                       Vehicle vehicle = (Vehicle) userAccount.getPerson();
//                        if (vehicle != null) {
//                            ArrayList<VehiclePartCondition> vehiclePartConditions = vehicle.getVehiclePartConditions();
//                            if (vehiclePartConditions != null && vehiclePartConditions.size() > 0) {
//                                String selection = VehicleCondition.EXCELLENT.toString();
//                                for (VehiclePartCondition vehiclePartCondition : vehiclePartConditions) {
//                                    if (vehiclePartCondition.getCondition() == VehicleCondition.BAD) {
//                                        selection = VehicleCondition.BAD.toString();
//                                        break;
//                                    } else if (vehiclePartCondition.getCondition() == VehicleCondition.NEED_ATTENTION) {
//                                        selection = VehicleCondition.NEED_ATTENTION.toString();
//                                    }
//                                }
//                                row[1] = selection;
//                            }
//
                        defaultTableModel.addRow(row);
//                        }
                    }

                }

                if (vehicleOrganization.getUserAccountManager().getUserAccountList().size() > 0) {
                    userTable.setRowSelectionInterval(0, 0);
                }

                if (vehicleOrganization.getUserAccountManager().getUserAccountList().isEmpty()) {
                    visibleUI(false);
                }

                populateConditionTable();
            } else {
                visibleUI(false);
            }
        }
    }

    private void visibleUI(boolean visible) {
        upcomingTitleLabel.setVisible(visible);
        upcomingLabel.setVisible(visible);
        conditionTitleLabel.setVisible(visible);
        conditionTable.setVisible(visible);
        apppointmentButton.setVisible(visible);
    }
    
    private void showError(String a_text){
        errorLabel1.setText(a_text);
        errorLabel1.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        userTable = new javax.swing.JTable();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        conditionTable = new javax.swing.JTable();
        conditionTitleLabel = new javax.swing.JLabel();
        upcomingTitleLabel = new javax.swing.JLabel();
        upcomingLabel = new javax.swing.JLabel();
        apppointmentButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        mmTextField = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        ddTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        yyTextField = new javax.swing.JTextField();
        hrTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        minTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        messageTextArea = new javax.swing.JTextArea();
        errorLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(162, 162, 162));
        jLabel1.setText("USERS INFO");

        userTable.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        userTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "USER"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        userTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(userTable);
        if (userTable.getColumnModel().getColumnCount() > 0) {
            userTable.getColumnModel().getColumn(0).setResizable(false);
        }

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setPreferredSize(new java.awt.Dimension(10, 50));

        conditionTable.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        conditionTable.setForeground(new java.awt.Color(162, 162, 162));
        conditionTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "PART", "CONDITION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(conditionTable);

        conditionTitleLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        conditionTitleLabel.setForeground(new java.awt.Color(162, 162, 162));
        conditionTitleLabel.setText("CONDITION");

        upcomingTitleLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        upcomingTitleLabel.setForeground(new java.awt.Color(162, 162, 162));
        upcomingTitleLabel.setText("Upcoming Appointment    : ");

        upcomingLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        upcomingLabel.setForeground(new java.awt.Color(162, 162, 162));
        upcomingLabel.setText("11/12/2013 4:30 PM");

        apppointmentButton.setBackground(new java.awt.Color(55, 153, 255));
        apppointmentButton.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        apppointmentButton.setForeground(new java.awt.Color(255, 255, 255));
        apppointmentButton.setText("FIX APPOINTMENT");
        apppointmentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                apppointmentButtonActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(162, 162, 162));
        jLabel2.setText("Date");

        mmTextField.setForeground(new java.awt.Color(162, 162, 162));
        mmTextField.setToolTipText("MM");

        jLabel8.setForeground(new java.awt.Color(162, 162, 162));
        jLabel8.setText("/");

        ddTextField.setForeground(new java.awt.Color(162, 162, 162));
        ddTextField.setToolTipText("DD");

        jLabel9.setForeground(new java.awt.Color(162, 162, 162));
        jLabel9.setText("/");

        yyTextField.setForeground(new java.awt.Color(162, 162, 162));
        yyTextField.setToolTipText("YYYY");

        hrTextField.setForeground(new java.awt.Color(162, 162, 162));
        hrTextField.setToolTipText("MM");

        jLabel10.setForeground(new java.awt.Color(162, 162, 162));
        jLabel10.setText(":");

        minTextField.setForeground(new java.awt.Color(162, 162, 162));
        minTextField.setToolTipText("DD");

        jLabel3.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(162, 162, 162));
        jLabel3.setText("MESSAGE");

        messageTextArea.setColumns(20);
        messageTextArea.setRows(5);
        jScrollPane3.setViewportView(messageTextArea);

        errorLabel1.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        errorLabel1.setForeground(new java.awt.Color(255, 0, 0));
        errorLabel1.setText("Error");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(267, 267, 267)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(conditionTitleLabel)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(upcomingTitleLabel)
                                .addGap(18, 18, 18)
                                .addComponent(upcomingLabel))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(apppointmentButton)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(48, 48, 48)
                                .addComponent(mmTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ddTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(yyTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(hrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(minTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(errorLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 449, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jSeparator3, javax.swing.GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(upcomingTitleLabel)
                                    .addComponent(upcomingLabel))
                                .addGap(47, 47, 47)
                                .addComponent(conditionTitleLabel)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(mmTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ddTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8)
                                    .addComponent(yyTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9)
                                    .addComponent(hrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(minTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(44, 44, 44)
                                        .addComponent(jLabel3)
                                        .addGap(69, 69, 69))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)))
                                .addComponent(apppointmentButton))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(errorLabel1)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(503, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void apppointmentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_apppointmentButtonActionPerformed

        int selectedItem = userTable.getSelectedRow();
        if (selectedItem > -1) {

            UserAccount vehicleUserAccount = (UserAccount) userTable.getValueAt(selectedItem, 0);
            if (vehicleUserAccount != null) {
                Vehicle vehicle = (Vehicle) vehicleUserAccount.getPerson();
                if (isValidationSuccessful() && vehicle != null) {
                    errorLabel1.setVisible(false);
                    VehicleServiceRequest vehicleServiceRequest = new VehicleServiceRequest();
                    vehicleServiceRequest.setMessage(messageTextArea.getText());
                    vehicleServiceRequest.setRequestDate(new Date());
                    vehicleServiceRequest.setRequestStatus(RequestStatus.REQUEST_RAISED);
                    vehicleServiceRequest.setSender(userAccount);
                    vehicleServiceRequest.setReceiver(vehicleUserAccount);
                    vehicleServiceRequest.setVehicleRequestType(VehicleRequestType.SERVICE);
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Integer.parseInt(yyTextField.getText()), Integer.parseInt(mmTextField.getText()), 
                            Integer.parseInt(ddTextField.getText()), Integer.parseInt(hrTextField.getText()), Integer.parseInt(minTextField.getText()));
                    Response response = vehicleUserAccount.getWorkQueue().addWorkRequest(vehicleServiceRequest);                  
                    if(response.getErrorCode() == ErrorCode.SUCCESS){
                        userAccount.getWorkQueue().addWorkRequest(vehicleServiceRequest);
                        vehicle.setUpcomingAppointment(calendar.getTime());
                        upcomingLabel.setText(vehicle.getUpcomingAppointment() == null ? "No Upcoming Appointment" : vehicle.getUpcomingAppointment().toString());
                        ServiceCentreOrganization centreOrganization = null;
                        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                            if(organization instanceof  ServiceCentreOrganization){
                                centreOrganization = (ServiceCentreOrganization)organization;
                                break;
                            }
                        }
                        centreOrganization.getWorkQueue().addWorkRequest(vehicleServiceRequest);
                        Toast.makeText(new JFrame(), "Appointment created successfully", Toast.LENGTH_SHORT, Toast.Style.SUCCESS).display();  
                        Map<BroadcastStateKey, Object> map = new HashMap<>();
                        map.put(BroadcastStateKey.BROADCAST_MESSAGE, BroadcastValue.SERVICE_CREATED_APPOINTMENT);
                        ConfigureASystem.getInstance().getManagerDelegate().sendBroadCast(map);
                    }else{
                         Toast.makeText(new JFrame(), "There was an error creating appointment", Toast.LENGTH_SHORT, Toast.Style.ERROR).display();
                    }
                }
            }
        }


    }//GEN-LAST:event_apppointmentButtonActionPerformed

    private boolean isValidationSuccessful() {

        if(!Validation.isMonthValid(mmTextField.getText())){
            showError("Please enter valid month");
            return false;
        }else if(!Validation.isDateValid(ddTextField.getText())){
            showError("Please enter valid date");
             return false;
        }else if(!Validation.isFutureYearValid(yyTextField.getText())){
            showError("Please enter valid year");
             return false;           
        }else if(!(messageTextArea != null && messageTextArea.getText().length() > 0)){
            showError("Please enter message");
             return false;
        }else if(!Validation.isHourValid(hrTextField.getText())){
            showError("Please select time 8:00 to 19:59");
             return false;           
        }else if(!Validation.isMinuteValid(minTextField.getText())){
            showError("Please select minute 0 to 59");
             return false;           
        }   
        else{
            Calendar calendar = Calendar.getInstance();
                    calendar.set(Integer.parseInt(yyTextField.getText()), Integer.parseInt(mmTextField.getText()), 
                            Integer.parseInt(ddTextField.getText()), Integer.parseInt(hrTextField.getText()), Integer.parseInt(minTextField.getText()));
                    if(!Validation.isDateFuture(calendar.getTime())){
                        showError("Please enter future date");
                        return false;
                    }                             
        }
        
        return true;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton apppointmentButton;
    private javax.swing.JTable conditionTable;
    private javax.swing.JLabel conditionTitleLabel;
    private javax.swing.JTextField ddTextField;
    private javax.swing.JLabel errorLabel1;
    private javax.swing.JTextField hrTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTextArea messageTextArea;
    private javax.swing.JTextField minTextField;
    private javax.swing.JTextField mmTextField;
    private javax.swing.JLabel upcomingLabel;
    private javax.swing.JLabel upcomingTitleLabel;
    private javax.swing.JTable userTable;
    private javax.swing.JTextField yyTextField;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onNotificationState(BroadcastMessage a_NotificationState, Map<BroadcastStateKey, Object> a_Parameters) { 
        if(a_NotificationState != null && a_NotificationState == BroadcastMessage.BROADCAST){
            if(a_Parameters != null && a_Parameters.containsKey(BroadcastStateKey.BROADCAST_MESSAGE) 
                    && (a_Parameters.get(BroadcastStateKey.BROADCAST_MESSAGE) instanceof BroadcastValue)){
                BroadcastValue broadcastValue = (BroadcastValue)a_Parameters.get(BroadcastStateKey.BROADCAST_MESSAGE);
                if((broadcastValue == BroadcastValue.VEHICLE_REQUEST_ACCEPTED) || (broadcastValue == BroadcastValue.VEHICLE_REQUEST_UPDATED)){
                  populateUserTable();
                }
            }
        }
    }
}
