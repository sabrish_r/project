/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.userinterface.service;

import com.neu.business.ConfigureASystem;
import com.neu.business.ErrorCode;
import com.neu.business.IManagerDelegate;
import com.neu.business.Response;
import com.neu.business.enterprise.Enterprise;
import com.neu.business.manager.BroadcastMessage;
import com.neu.business.manager.BroadcastStateKey;
import com.neu.business.manager.BroadcastValue;
import com.neu.business.manager.IBroadcastObserver;
import com.neu.business.organization.Organization;
import com.neu.business.organization.ServiceCentreOrganization;
import com.neu.business.useraccount.UserAccount;
import com.neu.utils.Toast;
import com.neu.business.workqueue.RequestStatus;
import com.neu.business.workqueue.VehicleServiceRequest;
import com.neu.business.workqueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sabrish
 */
public class ServiceUiWorkRequestPanel extends javax.swing.JPanel implements IBroadcastObserver{

    /**
     * Creates new form ServiceUiWorkRequestPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Enterprise enterprise;

    
    public ServiceUiWorkRequestPanel(JPanel userProcessContainer,Enterprise enterprise, UserAccount userAccount) {
        initComponents();
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        this.userProcessContainer = userProcessContainer;
        updateViews();
        IManagerDelegate managerDelegate = ConfigureASystem.getInstance().getManagerDelegate();
        managerDelegate.registerBroadcastObserver(this);
        addContainerListener(new ContainerAdapter() {

            @Override
            public void componentRemoved(ContainerEvent e) {
                managerDelegate.unregisterBroadcastObserver(ServiceUiWorkRequestPanel.this);
            }
                
                
            });
    }
    
    private void populateOrganisationTable(){
        
        if(enterprise != null && enterprise.getOrganizationDirectory() != null && enterprise.getOrganizationDirectory().getOrganizationList().size() > 0){
            Organization organization = null;
            for(Organization organization1 : enterprise.getOrganizationDirectory().getOrganizationList()){
                if(organization1 instanceof ServiceCentreOrganization){
                    organization = organization1;
                    break;
                }
            }
            ArrayList<WorkRequest> workRequestList = organization.getWorkQueue().getWorkRequestList();
            if(workRequestList != null ){
                DefaultTableModel defaultTableModel = (DefaultTableModel) organisationWorkRequestTable.getModel();
                defaultTableModel.setRowCount(0);
                for(WorkRequest workRequest : workRequestList){
                    VehicleServiceRequest vehicleServiceRequest = (VehicleServiceRequest)workRequest;
                    Object[] row = new Object[7];
                    row[0] = vehicleServiceRequest;
                    row[1] = vehicleServiceRequest.getRequestStatus().getValue();
                    row[2] = vehicleServiceRequest.getSender();
                    row[3] = vehicleServiceRequest.getReceiver();
                    row[4] = vehicleServiceRequest.getMessage();
                    row[5] = vehicleServiceRequest.getRequestDate();
                    row[6] = vehicleServiceRequest.getResolveDate()==null?"Pending":vehicleServiceRequest.getResolveDate();
                    defaultTableModel.addRow(row);
                }
            }
            
        }
    }
    
    private void populateServiceWorkqueue(){
        
        if(userAccount != null){
            ArrayList<WorkRequest> workRequestList = userAccount.getWorkQueue().getWorkRequestList();
            if(workRequestList != null ){
                DefaultTableModel defaultTableModel = (DefaultTableModel) myWorkRequestTable.getModel();
                defaultTableModel.setRowCount(0);
                for(WorkRequest workRequest : workRequestList){
                    VehicleServiceRequest vehicleServiceRequest = (VehicleServiceRequest)workRequest;
                    Object[] row = new Object[7];
                    row[0] = vehicleServiceRequest;
                    row[1] = vehicleServiceRequest.getRequestStatus().getValue();
                    row[2] = vehicleServiceRequest.getSender();
                    row[3] = vehicleServiceRequest.getReceiver();
                    row[4] = vehicleServiceRequest.getMessage();
                    row[5] = vehicleServiceRequest.getRequestDate();
                    row[6] = vehicleServiceRequest.getResolveDate()==null?"Pending":vehicleServiceRequest.getResolveDate();
                    defaultTableModel.addRow(row);
                }
            }
        }
    }
    
     private void updateViews(){
         
         myWorkRequestTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable target = (JTable) e.getSource();
                    int row = target.getSelectedRow();
                    if(row > -1){                      
                        VehicleServiceRequest serviceRequest = (VehicleServiceRequest)myWorkRequestTable.getValueAt(row, 0);
                        RequestUpdatePanel requestUpdatePanel = new RequestUpdatePanel(userProcessContainer, serviceRequest, enterprise, userAccount,false);
                        userProcessContainer.add("RequestUpdatePanel", requestUpdatePanel);
                        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
                        cardLayout.next(userProcessContainer);
                    }
                }
            }
        });
        nameLabel.setText(userAccount.getPerson().getName());
        populateOrganisationTable();
        populateServiceWorkqueue();
     }

     

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        myWorkRequestTable = new javax.swing.JTable();
        assignToMeButton = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        organisationWorkRequestTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        nameLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(162, 162, 162));
        jLabel1.setText("MY REQUEST");

        myWorkRequestTable.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        myWorkRequestTable.setForeground(new java.awt.Color(162, 162, 162));
        myWorkRequestTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "REQUEST", "STATUS", "SENDER", "RECEIVER", "MESSAGE", "REQUEST DATE", "RESOLVED DATE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(myWorkRequestTable);

        assignToMeButton.setBackground(new java.awt.Color(51, 102, 204));
        assignToMeButton.setForeground(new java.awt.Color(255, 255, 255));
        assignToMeButton.setText("ASSIGN TO ME");
        assignToMeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignToMeButtonActionPerformed(evt);
            }
        });

        organisationWorkRequestTable.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        organisationWorkRequestTable.setForeground(new java.awt.Color(162, 162, 162));
        organisationWorkRequestTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "REQUEST", "STATUS", "SENDER", "RECEIVER", "MESSAGE", "REQUEST DATE", "RESOLVED DATE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(organisationWorkRequestTable);

        jLabel2.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(162, 162, 162));
        jLabel2.setText("ORGANIZATION REQUEST");

        jLabel3.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(162, 162, 162));
        jLabel3.setText("Welcome,");

        nameLabel.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        nameLabel.setForeground(new java.awt.Color(162, 162, 162));
        nameLabel.setText("Sabrish");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(assignToMeButton)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(644, 644, 644))))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(31, 31, 31)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 796, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(nameLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(192, 192, 192)
                .addComponent(assignToMeButton)
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(86, 86, 86)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(283, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void assignToMeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignToMeButtonActionPerformed
        
        int selectedIndex = organisationWorkRequestTable.getSelectedRow();
        if(selectedIndex > -1){
            
            VehicleServiceRequest serviceRequest = (VehicleServiceRequest)organisationWorkRequestTable.getValueAt(selectedIndex, 0);
            if(serviceRequest != null){
               Response<WorkRequest> response = userAccount.getWorkQueue().addWorkRequest(serviceRequest);
               if(response.getErrorCode() == ErrorCode.SUCCESS){
                    serviceRequest.setReceiver(userAccount);
                    serviceRequest.setAssignDate(new Date());
                    serviceRequest.setRequestStatus(RequestStatus.REQUEST_ACCEPTED);
                     populateServiceWorkqueue();
                     populateOrganisationTable();
                     Toast.makeText(null, "Work Request Assigned successfully", Toast.LENGTH_SHORT, Toast.Style.SUCCESS).display();
                     Map<BroadcastStateKey, Object> map = new HashMap<>();
                     map.put(BroadcastStateKey.BROADCAST_MESSAGE, BroadcastValue.VEHICLE_REQUEST_ACCEPTED);
                     ConfigureASystem.getInstance().getManagerDelegate().sendBroadCast(map);
               }else{
                     Toast.makeText(null, "Work Request Already exist", Toast.LENGTH_SHORT, Toast.Style.ERROR).display();
               }
              
            }
        }
    }//GEN-LAST:event_assignToMeButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignToMeButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable myWorkRequestTable;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JTable organisationWorkRequestTable;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onNotificationState(BroadcastMessage a_NotificationState, Map<BroadcastStateKey, Object> a_Parameters) {
         if(a_NotificationState != null && a_NotificationState == BroadcastMessage.BROADCAST){
            if(a_Parameters != null && a_Parameters.containsKey(BroadcastStateKey.BROADCAST_MESSAGE) && (a_Parameters.get(BroadcastStateKey.BROADCAST_MESSAGE) instanceof BroadcastValue)){
                BroadcastValue broadcastValue = (BroadcastValue)a_Parameters.get(BroadcastStateKey.BROADCAST_MESSAGE);
                if((broadcastValue == BroadcastValue.SERVICE_CREATED_APPOINTMENT) || (broadcastValue == BroadcastValue.VEHICLE_REQUEST_UPDATED)){
                   populateOrganisationTable();
                   populateServiceWorkqueue();
                }
            }
        }
    }
}
