/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.userinterface.admin;

import java.awt.Frame;
import javax.swing.JDialog;

/**
 *
 * @author Sabrish
 */
public class CustomDialog {
    
    public static JDialog getAddNetworkDialog(){
        
        JDialog dialog = new JDialog();
        CreateNetworkDialogPanel createNetworkDialogPanel = new CreateNetworkDialogPanel();
        createNetworkDialogPanel.setSize(250, 200);
        dialog.setContentPane(createNetworkDialogPanel);
        
        return dialog;
        
    }
}
