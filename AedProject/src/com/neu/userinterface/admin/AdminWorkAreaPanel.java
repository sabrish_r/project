/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.userinterface.admin;

import com.neu.business.ConfigureASystem;
import com.neu.business.ErrorCode;
import com.neu.business.IManagerDelegate;
import com.neu.business.Response;
import com.neu.business.enterprise.CreateAdminPanel;
import com.neu.business.enterprise.Enterprise;
import com.neu.business.enterprise.IEnterpriseManager;
import com.neu.business.network.INetworkManager;
import com.neu.business.network.Network;
import com.neu.userinterface.LoginJPanel;
import com.neu.utils.Toast;
import com.neu.utils.Validation;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sabrish
 */
public class AdminWorkAreaPanel extends javax.swing.JPanel {

    /**
     * Creates new form AdminWorkAreaPanel
     */
    
    private IManagerDelegate managerDelegate;
    private JPanel userProcessContainer;
    
    public AdminWorkAreaPanel(JPanel userProcessContainer) {
        initComponents();
        managerDelegate = ConfigureASystem.getInstance().getManagerDelegate();
        this.userProcessContainer = userProcessContainer;
        networkErrorLabel.setVisible(false);
        populateNetworkTable();
        updateNetworkTextListener();
    }
    
    private void updateNetworkTextListener(){
        
        
        networkTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
               if (!e.getValueIsAdjusting()) {
                    onRowSelectionChanged();
                }
            }
        });
                
                
        addNetworkTextField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
               
                onNetworkTextListenerUpdate();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
               onNetworkTextListenerUpdate();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                onNetworkTextListenerUpdate();
            }
        });
    }
    
    private void onRowSelectionChanged(){
        int selectedItem = networkTable.getSelectedRow();
        
        if(selectedItem > -1){
            
            Network network = (Network) networkTable.getValueAt(selectedItem, 0);
            if(network != null){
                networkNameLabel.setText(network.getName());
            }
            
        }
        populateEnterpriseTable();
    }
    
    private void onNetworkTextListenerUpdate(){
        
        if(addNetworkTextField.getText().length() > 0){
            addButton.setEnabled(true);
        }else{
             addButton.setEnabled(false);
        }
    }
    
    private void populateNetworkTable(){
        
        INetworkManager iNetworkManager = managerDelegate.getNetworkManager();
        if(iNetworkManager != null){
        ArrayList<Network> networks = iNetworkManager.getNetworkList();
        if(networks != null && networks.size() >= 0){
            visibleUI(true);
            DefaultTableModel defaultTableModel = (DefaultTableModel)networkTable.getModel();
            defaultTableModel.setRowCount(0);
            for(Network network : networks){
                Object[] row = new Object[1];
                row[0] = network;
               
                defaultTableModel.addRow(row);
                
            }
           if(networks.size() > 0) {
           networkTable.setRowSelectionInterval(0, 0);
           }
           
           if(networks.isEmpty())
               visibleUI(false);
           
           populateEnterpriseTable();
        }else{
            visibleUI(false);
        }
      }
    }
    
    public void populateEnterpriseTable(){
        
        int selectedItem = networkTable.getSelectedRow();
        
        if(selectedItem > -1){
            
            Network network = (Network) networkTable.getValueAt(selectedItem, 0);
            if(network != null){
                
                IEnterpriseManager iEnterpriseManager = network.getEnterpriseDirectory();
                if(iEnterpriseManager != null){
                    ArrayList<Enterprise> enterprises = iEnterpriseManager.getEnterpriseList();
                    if(enterprises != null){
                        DefaultTableModel defaultTableModel = (DefaultTableModel)enterPriseTable.getModel();
                        defaultTableModel.setRowCount(0);
                        for(Enterprise enterprise : enterprises){
                             Object[] row = new Object[1];
                             row[0] = enterprise;
                             defaultTableModel.addRow(row);
                        }
                    }
                }
            }
            
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        networkTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        deleteNetworkButton = new javax.swing.JButton();
        enterpriseDirectoryLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        addNetworkTextField = new javax.swing.JTextField();
        addButton = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        networkNameLabel = new javax.swing.JLabel();
        addEnterPriseButton = new javax.swing.JButton();
        deleteEnterpriseButton = new javax.swing.JButton();
        logOutButton = new javax.swing.JButton();
        networkErrorLabel = new javax.swing.JLabel();
        tablePanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        enterPriseTable = new javax.swing.JTable();
        addAdminButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(23, 111, 152));
        setLayout(new java.awt.GridBagLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        networkTable.setForeground(new java.awt.Color(162, 162, 162));
        networkTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Networks"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        networkTable.setSelectionBackground(new java.awt.Color(255, 255, 255));
        networkTable.setSelectionForeground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setViewportView(networkTable);
        if (networkTable.getColumnModel().getColumnCount() > 0) {
            networkTable.getColumnModel().getColumn(0).setResizable(false);
        }

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(162, 162, 162));
        jLabel1.setText("Network INFORMATION");

        deleteNetworkButton.setBackground(new java.awt.Color(53, 153, 255));
        deleteNetworkButton.setForeground(new java.awt.Color(255, 255, 255));
        deleteNetworkButton.setText("DELETE");
        deleteNetworkButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteNetworkButtonActionPerformed(evt);
            }
        });

        enterpriseDirectoryLabel.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        enterpriseDirectoryLabel.setForeground(new java.awt.Color(162, 162, 162));
        enterpriseDirectoryLabel.setText("ENTERPRISE DIRECTORY");

        jLabel3.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(162, 162, 162));
        jLabel3.setText("Name");

        addNetworkTextField.setForeground(new java.awt.Color(204, 204, 204));

        addButton.setBackground(new java.awt.Color(53, 153, 255));
        addButton.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        addButton.setForeground(new java.awt.Color(255, 255, 255));
        addButton.setText("+");
        addButton.setEnabled(false);
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setPreferredSize(new java.awt.Dimension(10, 50));

        jLabel4.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(162, 162, 162));
        jLabel4.setText("ADD NETWORK");

        networkNameLabel.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        networkNameLabel.setForeground(new java.awt.Color(162, 162, 162));
        networkNameLabel.setText("NETWORK 1");

        addEnterPriseButton.setBackground(new java.awt.Color(53, 153, 255));
        addEnterPriseButton.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        addEnterPriseButton.setForeground(new java.awt.Color(255, 255, 255));
        addEnterPriseButton.setText("ADD ENTERPRISE");
        addEnterPriseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addEnterPriseButtonActionPerformed(evt);
            }
        });

        deleteEnterpriseButton.setBackground(new java.awt.Color(53, 153, 255));
        deleteEnterpriseButton.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        deleteEnterpriseButton.setForeground(new java.awt.Color(255, 255, 255));
        deleteEnterpriseButton.setText("DELETE ENTERPRISE");
        deleteEnterpriseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteEnterpriseButtonActionPerformed(evt);
            }
        });

        logOutButton.setBackground(new java.awt.Color(51, 153, 255));
        logOutButton.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        logOutButton.setForeground(new java.awt.Color(255, 255, 255));
        logOutButton.setText("LOGOUT");
        logOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logOutButtonActionPerformed(evt);
            }
        });

        networkErrorLabel.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        networkErrorLabel.setForeground(new java.awt.Color(255, 0, 0));
        networkErrorLabel.setText("Error");

        enterPriseTable.setForeground(new java.awt.Color(162, 162, 162));
        enterPriseTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        enterPriseTable.setSelectionBackground(new java.awt.Color(255, 255, 255));
        enterPriseTable.setSelectionForeground(new java.awt.Color(204, 204, 204));
        jScrollPane2.setViewportView(enterPriseTable);
        if (enterPriseTable.getColumnModel().getColumnCount() > 0) {
            enterPriseTable.getColumnModel().getColumn(0).setResizable(false);
        }

        javax.swing.GroupLayout tablePanelLayout = new javax.swing.GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        tablePanelLayout.setVerticalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        addAdminButton.setBackground(new java.awt.Color(53, 153, 255));
        addAdminButton.setFont(new java.awt.Font("SansSerif", 1, 11)); // NOI18N
        addAdminButton.setForeground(new java.awt.Color(255, 255, 255));
        addAdminButton.setText("ADD ADMIN");
        addAdminButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAdminButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator3, javax.swing.GroupLayout.DEFAULT_SIZE, 781, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(287, 287, 287)
                        .addComponent(jLabel1)
                        .addGap(172, 172, 172)
                        .addComponent(logOutButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(networkErrorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel3)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(addNetworkTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(26, 26, 26)
                                                .addComponent(addButton))
                                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(33, 33, 33)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(enterpriseDirectoryLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(addEnterPriseButton)
                                        .addGap(18, 18, 18)
                                        .addComponent(deleteEnterpriseButton)
                                        .addGap(18, 18, 18)
                                        .addComponent(addAdminButton))
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(networkNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(89, 89, 89)
                                            .addComponent(deleteNetworkButton))
                                        .addComponent(tablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addGap(26, 26, 26))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(logOutButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(10, 10, 10)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(13, 13, 13)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(addNetworkTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(addButton))
                                    .addComponent(jLabel3))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(networkNameLabel)
                            .addComponent(deleteNetworkButton))
                        .addGap(18, 18, 18)
                        .addComponent(enterpriseDirectoryLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(deleteEnterpriseButton)
                            .addComponent(addEnterPriseButton)
                            .addComponent(addAdminButton))
                        .addGap(173, 173, 173)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(networkErrorLabel)
                .addGap(47, 47, 47))
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 58;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(25, 37, 0, 0);
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        
        String networkName = addNetworkTextField.getText();
        if(Validation.isStringValid(networkName)){
             INetworkManager iNetworkManager = managerDelegate.getNetworkManager();
             Response<Network> response = iNetworkManager.createAndAddNetwork(networkName);
             if(response.getErrorCode() == ErrorCode.SUCCESS){
                  Toast.makeText(new JFrame(), "Network created successfully", Toast.LENGTH_SHORT, Toast.Style.SUCCESS).display();
                   networkErrorLabel.setVisible(false);
                   addNetworkTextField.setText("");
             }else if(response.getErrorCode() == ErrorCode.ALREADY_EXIST){
                 showError("This Network already exist. Please create network with different name");
             }else{
                 showError("Something went wrong. Please try again");
             }
            
            
             populateNetworkTable();
            
        }else{
            showError("Please enter valid network name - only character, numbers, _");
            networkErrorLabel.setVisible(true);
        }
        
         addNetworkTextField.setText("");
    }//GEN-LAST:event_addButtonActionPerformed

    private void showError(String a_text){
        networkErrorLabel.setText(a_text);
            networkErrorLabel.setVisible(true);
    }
    private void deleteNetworkButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteNetworkButtonActionPerformed
        int selectedItem = networkTable.getSelectedRow();        
        if(selectedItem > -1){
           Network network = (Network)networkTable.getValueAt(selectedItem, 0);
           if(network != null){
                INetworkManager iNetworkManager = managerDelegate.getNetworkManager();
                iNetworkManager.deleteNetwork(network);
                populateNetworkTable();
                Toast.makeText(new JFrame(), "Network deleted successfully", Toast.LENGTH_SHORT, Toast.Style.SUCCESS).display();
           }
        }else{
            //show error
        }
    }//GEN-LAST:event_deleteNetworkButtonActionPerformed

    private void addEnterPriseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addEnterPriseButtonActionPerformed
        int selectedItem = networkTable.getSelectedRow();        
        if(selectedItem > -1){
        Network network = (Network)networkTable.getValueAt(selectedItem, 0);
        CreateEnterpriseJPanel  mainWorkAreaJPanel = new CreateEnterpriseJPanel(userProcessContainer, network);
        userProcessContainer.add("CreateEnterpriseJPanel",mainWorkAreaJPanel);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.next(userProcessContainer);
        }else{
            Toast.makeText(new JFrame(), "Please select a Network", Toast.LENGTH_SHORT, Toast.Style.ERROR).display();
        }
    }//GEN-LAST:event_addEnterPriseButtonActionPerformed

    private void deleteEnterpriseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteEnterpriseButtonActionPerformed
       
        int selectedItem = networkTable.getSelectedRow();        
        if(selectedItem > -1){
        Network network = (Network)networkTable.getValueAt(selectedItem, 0);
        int enterpriseSelectedItem = enterPriseTable.getSelectedRow();  
        if(network != null && enterpriseSelectedItem > -1){           
            IEnterpriseManager enterpriseManager = network.getEnterpriseDirectory();
            if(enterpriseManager != null){        
            Enterprise enterprise = (Enterprise)enterPriseTable.getValueAt(enterpriseSelectedItem, 0);
            enterpriseManager.deleteEnterprise(enterprise);
            Toast.makeText(new JFrame(), "Enterprise deleted successfully", Toast.LENGTH_SHORT, Toast.Style.SUCCESS).display();
            populateEnterpriseTable();
            }
          }else{
            Toast.makeText(new JFrame(), "Please select a row", Toast.LENGTH_SHORT, Toast.Style.ERROR).display();
        }
        }
    }//GEN-LAST:event_deleteEnterpriseButtonActionPerformed

    private void logOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logOutButtonActionPerformed
      
        userProcessContainer.removeAll();
        LoginJPanel loginJPanel = new LoginJPanel(userProcessContainer);
        userProcessContainer.add("LoginJPanel",loginJPanel);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.next(userProcessContainer);
        
    }//GEN-LAST:event_logOutButtonActionPerformed

    private void addAdminButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAdminButtonActionPerformed
        
        int selectedItem = networkTable.getSelectedRow();        
        if(selectedItem > -1){
        Network network = (Network)networkTable.getValueAt(selectedItem, 0);
        int enterpriseSelectedItem = enterPriseTable.getSelectedRow();  
        if(network != null && enterpriseSelectedItem > -1){           
            IEnterpriseManager enterpriseManager = network.getEnterpriseDirectory();
            if(enterpriseManager != null){        
            Enterprise enterprise = (Enterprise)enterPriseTable.getValueAt(enterpriseSelectedItem, 0);
            CreateAdminPanel  mainWorkAreaJPanel = new CreateAdminPanel(userProcessContainer, enterprise);
            userProcessContainer.add("CreateAdminPanel",mainWorkAreaJPanel);
            CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
            cardLayout.next(userProcessContainer);
            }
          }else{
            Toast.makeText(new JFrame(), "Please select a row", Toast.LENGTH_SHORT, Toast.Style.ERROR).display();
        }
        }
    }//GEN-LAST:event_addAdminButtonActionPerformed

    private void visibleUI(boolean visible){
        networkNameLabel.setVisible(visible);
        tablePanel.setVisible(visible);
        deleteNetworkButton.setVisible(visible);
        deleteEnterpriseButton.setVisible(visible);
        addEnterPriseButton.setVisible(visible);
        enterpriseDirectoryLabel.setVisible(visible);
        addAdminButton.setVisible(visible);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addAdminButton;
    private javax.swing.JButton addButton;
    private javax.swing.JButton addEnterPriseButton;
    private javax.swing.JTextField addNetworkTextField;
    private javax.swing.JButton deleteEnterpriseButton;
    private javax.swing.JButton deleteNetworkButton;
    private javax.swing.JTable enterPriseTable;
    private javax.swing.JLabel enterpriseDirectoryLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JButton logOutButton;
    private javax.swing.JLabel networkErrorLabel;
    private javax.swing.JLabel networkNameLabel;
    private javax.swing.JTable networkTable;
    private javax.swing.JPanel tablePanel;
    // End of variables declaration//GEN-END:variables
}
