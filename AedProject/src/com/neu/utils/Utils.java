/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.utils;

import com.neu.business.ConfigureASystem;
import com.neu.business.manager.BroadcastStateKey;
import com.neu.business.vehicle.Address;
import com.neu.business.vehicle.Location;
import com.neu.business.vehicle.Notification;
import com.neu.business.vehicle.NotificationType;
import com.neu.business.vehicle.VehicleCondition;
import com.neu.business.vehicle.VehiclePart;
import com.neu.business.vehicle.VehiclePartCondition;
import com.sun.mail.smtp.SMTPTransport;
import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import java.security.Security;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Sabrish
 */
public class Utils {
    
    /**
     * Send email using GMail SMTP server.
     *
     * @param username GMail username
     * @param password GMail password
     * @param recipientEmail TO recipient
     * @param title title of the message
     * @param message message to be sent
     * @throws AddressException if the email address parse failed
     * @throws MessagingException if the connection is dead or not in the connected state or if the message is not a MimeMessage
     */
    public static void Send(final String username, final String password, String recipientEmail, String title, String message){
        Utils.Send(username, password, recipientEmail, "", title, message);
    }

    /**
     * Send email using GMail SMTP server.
     *
     * @param username GMail username
     * @param password GMail password
     * @param recipientEmail TO recipient
     * @param ccEmail CC recipient. Can be empty if there is no CC recipient
     * @param title title of the message
     * @param message message to be sent
     * @throws AddressException if the email address parse failed
     * @throws MessagingException if the connection is dead or not in the connected state or if the message is not a MimeMessage
     */
    public static void Send(final String username, final String password, String recipientEmail, String ccEmail, String title, String message) {
        try{
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

        // Get a Properties object
        Properties props = System.getProperties();
        props.setProperty("mail.smtps.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        props.setProperty("mail.smtps.auth", "true");

        /*
        If set to false, the QUIT command is sent and the connection is immediately closed. If set 
        to true (the default), causes the transport to wait for the response to the QUIT command.

        ref :   http://java.sun.com/products/javamail/javadocs/com/sun/mail/smtp/package-summary.html
                http://forum.java.sun.com/thread.jspa?threadID=5205249
                smtpsend.java - demo program from javamail
        */
        props.put("mail.smtps.quitwait", "false");

        Session session = Session.getInstance(props, null);

        
        // -- Create a new message --
        final MimeMessage msg = new MimeMessage(session);

        // -- Set the FROM and TO fields --
        msg.setFrom(new InternetAddress(username + "@gmail.com"));
        msg.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));
        msg.setSubject(title);
        msg.setText(message, "utf-8");
        msg.setSentDate(new Date());

        SMTPTransport t = (SMTPTransport)session.getTransport("smtps");

        t.connect("smtp.gmail.com", username, password);
        t.sendMessage(msg, msg.getAllRecipients());      
        t.close();
        }catch(Exception e){
            System.out.print(e.getMessage());
        }
    }
    private static Address address1 = new Address(02115,"Boston","MA", "Northeastern university");
    private static Address address2 = new Address(02115,"Boston","MA", "Prudential centre");
    private static Address address3 = new Address(02120,"Boston","MA", "Cityview Apartment");
    private static Address address4 = new Address(2138,"cambridge","MA", "Harvard University");
    private static Address address5 = new Address(2139,"Boston","MA", "Massachusetts Institute of Technology");
    
    private static Address[] addresses = {address1,address2,address3,address4,address5};
    
    public static Notification getSensorNotification(){
        int count = 0;
        while(count <= 5){
            Notification notification = new Notification();
            notification.setNotificationType(NotificationType.CRITICAL_ISSUE);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis() + ((count+1)*3600000));
            notification.setUpcomingAppointment(calendar.getTime());
            int addressCount = 0;
            if(addresses.length > count){ 
            addressCount = count;              
            }else{
                addressCount = 0;
            }
            Location location = new Location();
            location.setAddress(addresses[addressCount]);
            notification.addLocation(location);
            
            if((addressCount + 1) > count){
                addressCount = 0;
            }
            location = new Location();
            location.setAddress(addresses[addressCount+1]);
            notification.addLocation(location);
                   
            Map<BroadcastStateKey, Object> map = new HashMap<>();
            map.put(BroadcastStateKey.BROADCAST_VEHICLE_NOTIFICATION,notification);
            int vehicleConditionCount = 0;
            if(VehiclePart.values().length > count){
                vehicleConditionCount = count;
            }else{
                vehicleConditionCount = 0;
            }
            VehiclePartCondition vehiclePartCondition = new VehiclePartCondition();
            vehiclePartCondition.setVehiclePart(VehiclePart.values()[vehicleConditionCount]);
            vehiclePartCondition.setCondition(VehicleCondition.BAD);
            map.put(BroadcastStateKey.BROADCAST_VEHICLE_PART_STATUS,vehiclePartCondition);
            notification.setMessage(String.format("Critical issue found in %s!! Please go to the nearest service centre", VehiclePart.values()[vehicleConditionCount].name()));
            ConfigureASystem.getInstance().getManagerDelegate().sendBroadCast(map);
            count++;
            try{
            Thread.sleep(5000);
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
        }
        return null;
    }
}
