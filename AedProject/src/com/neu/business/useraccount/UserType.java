/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.useraccount;

/**
 *
 * @author Sabrish
 */
public enum UserType {
    
    ECOSYTEM_ADMIN,
    NETWORK_ADMIN,
    ENTERPRISE_ADMIN,
    ORGANISATION_EMPLOYEE,
    VEHICLE_USER
}
