/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.useraccount;

import com.neu.business.EcoSystem;
import com.neu.business.ErrorCode;
import com.neu.business.Response;
import com.neu.business.role.Role;
import com.neu.business.manager.IUserAccountManager;
import java.util.ArrayList;

/**
 *
 * @author Sabrish
 */
public class UserAccountDirectory implements IUserAccountManager{
    
    private ArrayList<UserAccount> userAccounts;
    
    public UserAccountDirectory (){   
        userAccounts = new ArrayList<>();
    }
    
     @Override
     public Response createUserAccount(String username, String password, UserType userType, User person, Role role){
         
         Response<UserAccount> response = new Response<>();
         ErrorCode errorCode = checkIfUserAccountExist(username);
         response.setErrorCode(errorCode);
         if(errorCode == ErrorCode.SUCCESS){
            UserAccount userAccount = new UserAccount();
            userAccount.setUsername(username);
            userAccount.setPassword(password);
            userAccount.setUserType(userType);
            userAccount.setRole(role);
            userAccount.setPerson(person);
            userAccounts.add(userAccount);
            EcoSystem.getInstance().getUserAccountManager().addUserAccount(userAccount);
            response.setResponse(userAccount);
         }
         
        
         return response;
     }

    @Override
    public UserAccount login(String username, String password) {
        
        if(userAccounts != null && userAccounts.size() > 0){
         for (UserAccount userAccount :  userAccounts)
            if (userAccount.getUsername().equals(username) && userAccount.getPassword().equals(password)){
                return userAccount;
            }
        }
        return null;
    }
    

    @Override
    public void addUserAccount(UserAccount userAccount) {
       if(userAccounts != null){
           userAccounts.add(userAccount);
       }
    }

    @Override
    public ArrayList<UserAccount> getUserAccountList() {
        return userAccounts;
    }

    @Override
    public boolean removeUserAccount(UserAccount userAccount) {
        
        if(userAccount != null && userAccounts.size() > 0 && userAccounts.contains(userAccount)){
            userAccounts.remove(userAccount);
            return true;
        }
        
        return false;
    }
    
    
    @Override
    public ErrorCode checkIfUserAccountExist(String name){
        ArrayList<UserAccount> userAccounts = EcoSystem.getInstance().getUserAccountManager().getUserAccountList();
        if(userAccounts != null && userAccounts.size() > 0){
            for(UserAccount userAccount : userAccounts){
                if(userAccount.getUsername().equals(name)){
                    return ErrorCode.USERNAME_EXIST;
                }
            }
        }
        return ErrorCode.SUCCESS;
    }
    
}
