/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business;

/**
 *
 * @author Sabrish
 */
public class ConfigureASystem {
    
   private  IManagerDelegate managerDelegate;
   
   private static ConfigureASystem configureASystem;
   public  ConfigureASystem(){
        managerDelegate = new ManagerDelegate();
   }
   
   public static ConfigureASystem getInstance(){
       if(configureASystem == null){
           configureASystem = new ConfigureASystem();
       }
       
       return configureASystem;
   }
   
   
    public  EcoSystem configure(){
        return getManagerDelegate().configureEcoSystem();      
    }
   
    
    public  IManagerDelegate getManagerDelegate(){
        return managerDelegate;
    }
}
