/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business;

import com.neu.business.manager.BroadcastStateKey;
import com.neu.business.manager.BroadcastValue;
import com.neu.business.manager.IBroadcastObserver;
import com.neu.business.manager.IUserAccountManager;
import com.neu.business.network.INetworkManager;
import java.util.Map;

/**
 *
 * @author Sabrish
 */
public interface IManagerDelegate {
    
    public EcoSystem configureEcoSystem();
    public EcoSystem getEcoSystem();
    public IUserAccountManager getUserAccountManger();
    public INetworkManager getNetworkManager();
    public void sendBroadCast(Map<BroadcastStateKey, Object> a_Parameters );
    public void registerBroadcastObserver(IBroadcastObserver broadcastObserver);
    public void unregisterBroadcastObserver(IBroadcastObserver broadcastObserver);
}
