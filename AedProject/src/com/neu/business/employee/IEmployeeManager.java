/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.employee;

import com.neu.business.useraccount.UserAccount;
import com.neu.business.useraccount.UserType;
import java.util.ArrayList;

/**
 *
 * @author Sabrish
 */
public interface IEmployeeManager {
    
     public Employee createEmployee(String name);
     public UserAccount createUserAccount(String username, String password, UserType userType, Employee employee);
     public ArrayList<Employee>  getEmployeeDirectory();
     public boolean removeEmployee(Employee employee);
}
