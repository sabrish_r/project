/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.employee;

import com.neu.business.useraccount.UserAccount;
import com.neu.business.useraccount.UserType;
import java.util.ArrayList;


public class EmployeeDirectory implements IEmployeeManager{
    
    private ArrayList<Employee> employeeList;

    public EmployeeDirectory() {
        employeeList = new ArrayList<>();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }
    
    @Override
    public Employee createEmployee(String name){
        Employee employee = new Employee();
        employee.setName(name);
        employeeList.add(employee);
        return employee;
    }

    @Override
    public UserAccount createUserAccount(String username, String password, UserType userType, Employee employee) {
         UserAccount userAccount = new UserAccount();
         userAccount.setUsername(username);
         userAccount.setPassword(password);
         userAccount.setUserType(userType);
         return userAccount;
    }

    @Override
    public ArrayList<Employee>  getEmployeeDirectory() {
        return employeeList;
    }

    @Override
    public boolean removeEmployee(Employee employee) {
       
        if(employee != null && employeeList.size() > 0 && employeeList.contains(employee)){
            employeeList.remove(employee);
            return true;
        }
        return false;
    }
    
    
}