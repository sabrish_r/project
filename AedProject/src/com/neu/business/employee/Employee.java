/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.employee;

import com.neu.business.useraccount.User;


public class Employee extends User{
    
    private int empId;
    private static int count = 1;

    public Employee() {
        empId = count;
        count++;
    }

    
    public int getId() {
        return empId;
    }


    @Override
    public String toString() {
        return String.valueOf(empId);
    }
    
    
}
