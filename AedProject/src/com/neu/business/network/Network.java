/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.network;

import com.neu.business.enterprise.EnterpriseDirectory;
import com.neu.business.enterprise.IEnterpriseManager;


/**
 *
 * @author raunak
 */
public class Network {
    
    private String name;
    private IEnterpriseManager enterpriseDirectory;

    public Network() {
        enterpriseDirectory = new EnterpriseDirectory();
    }

    public IEnterpriseManager getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
    
}
