/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.network;

import com.neu.business.Response;
import java.util.ArrayList;

/**
 *
 * @author Sabrish
 */
public interface INetworkManager {
    
      public Response createAndAddNetwork(String name);
      public ArrayList<Network> getNetworkList();
      public boolean deleteNetwork(Network network);
}
