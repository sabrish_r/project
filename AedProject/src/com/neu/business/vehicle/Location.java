/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.vehicle;

/**
 *
 * @author Sabrish
 */
public class Location {
 
    private String latitude;
    private String longitute;
    private Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

  
    public String getLatitude() {
        return "42.345453";
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitute() {
        return "-71.076486";
    }

    public void setLongitute(String longitute) {
        this.longitute = longitute;
    }
}
