/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.vehicle;

import com.neu.business.useraccount.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sabrish
 */
public class Vehicle extends User{
    
    private VehicleType vehicleType;
    private String vehicleNumber;
    private Map<VehiclePart,VehiclePartCondition> vehiclePartConditions;
    private ArrayList<Notification> notifications;
    private Date upcomingAppointment;
    private Location location;

    public Vehicle(){
        vehiclePartConditions = new HashMap<>();
        location = new Location();
        notifications = new ArrayList<>();
        setUpVehicleCondition();
    }
    private void setUpVehicleCondition(){
        
        for(int i=0; i<VehiclePart.values().length;i++){
        VehiclePartCondition vehiclePartCondition = new VehiclePartCondition();
        vehiclePartCondition.setVehiclePart(VehiclePart.values()[i]);
        vehiclePartCondition.setCondition(VehicleCondition.EXCELLENT);
        vehiclePartConditions.put(VehiclePart.values()[i],vehiclePartCondition);
        }
        
    }
    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public Map<VehiclePart,VehiclePartCondition> getVehiclePartConditions() {
        return vehiclePartConditions;
    }

    public void setVehiclePartConditions(Map<VehiclePart,VehiclePartCondition>vehiclePartConditions) {
        this.vehiclePartConditions = vehiclePartConditions;
    }
    
    public void addVehiclePartCondition(VehiclePartCondition vehiclePartCondition){
        if(vehiclePartConditions != null && vehiclePartCondition != null){
            vehiclePartConditions.put(vehiclePartCondition.getVehiclePart(), vehiclePartCondition);
        }
    }
    
     public void addResolvedVehiclePartCondition(VehiclePart vehiclePart,VehicleCondition vehicleCondition ){
        if(vehiclePart != null && vehicleCondition != null && vehiclePartConditions != null){
            VehiclePartCondition vehiclePartCondition = new VehiclePartCondition();
            vehiclePartCondition.setCondition(vehicleCondition);
            vehiclePartCondition.setVehiclePart(vehiclePart);
            vehiclePartConditions.put(vehiclePartCondition.getVehiclePart(), vehiclePartCondition);
        }
    }

    public Date getUpcomingAppointment() {
        return upcomingAppointment;
    }

    public void setUpcomingAppointment(Date upcomingAppointment) {
        this.upcomingAppointment = upcomingAppointment;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ArrayList<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<Notification> notifications) {
        this.notifications = notifications;
    }
    
    public Notification addNotification(){
        Notification notification = new Notification();
        if(notifications != null){
            notifications.add(notification);
        }
        
        return notification;
    }

     public Notification addNotification(Notification notification){
        if(notifications != null && !notifications.contains(notification)){
            notifications.add(0,notification);
        }
        
        return notification;
    }
}
