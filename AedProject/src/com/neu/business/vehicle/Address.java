/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.vehicle;

/**
 *
 * @author Sabrish
 */
public class Address {
    
    private long zipcode;
    private String city;
    private String state;
    private String address1;

    public Address() {
    }

    public Address(long zipcode, String city, String state, String address1) {
        this.zipcode = zipcode;
        this.city = city;
        this.state = state;
        this.address1 = address1;
    }
    

    public long getZipcode() {
        return zipcode;
    }

    public void setZipcode(long zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }
    
    
}
