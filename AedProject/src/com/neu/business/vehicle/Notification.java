/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.vehicle;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Sabrish
 */
public class Notification {
    
    private NotificationType notificationType;
    private String message;
    private Date upcomingAppointment;
    private ArrayList<Location> locations;
    
    public Notification(){      
        locations = new ArrayList<>();
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getUpcomingAppointment() {
        return upcomingAppointment;
    }

    public void setUpcomingAppointment(Date upcomingAppointment) {
        this.upcomingAppointment = upcomingAppointment;
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
    }
    
    public Location addLocation(){
        
        Location location = new Location();
        if(locations != null){
            locations.add(location);
        }
        return location;
    }
    
     public Location addLocation(Location location){
        if(locations != null && location != null && !locations.contains(location)){
            locations.add(location);
        }
        return location;
    }

    @Override
    public String toString() {
        return getMessage();
    }
     
     
    
}
