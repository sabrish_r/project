/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.vehicle;

/**
 *
 * @author Sabrish
 */
public enum VehicleCondition {
    
    EXCELLENT,
    NEED_ATTENTION,
    BAD
}
