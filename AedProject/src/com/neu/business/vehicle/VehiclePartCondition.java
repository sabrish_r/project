/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.vehicle;

/**
 *
 * @author Sabrish
 */
public class VehiclePartCondition {
    
   
    private VehiclePart vehiclePart;
    private VehicleCondition condition;

    public VehiclePart getVehiclePart() {
        return vehiclePart;
    }

    public void setVehiclePart(VehiclePart vehiclePart) {
        this.vehiclePart = vehiclePart;
    }

    public VehicleCondition getCondition() {
        return condition;
    }

    public void setCondition(VehicleCondition condition) {
        this.condition = condition;
    }

    @Override
    public String toString() {
        return getVehiclePart().name();
    }
    
    
    
    
}
