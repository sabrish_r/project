/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.workqueue;

import com.neu.business.vehicle.VehicleCondition;
import com.neu.business.vehicle.VehiclePart;
import com.neu.business.vehicle.VehiclePartCondition;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sabrish
 */
public class VehicleServiceRequest extends WorkRequest{
    
    private VehicleRequestType vehicleRequestType;
    private RequestStatus requestStatus;
    private Map<VehiclePart,VehiclePartCondition> resolvedVehiclePartConditions;
   

    public VehicleServiceRequest(){
        resolvedVehiclePartConditions = new HashMap<>();
    }
    
    public VehicleRequestType getVehicleRequestType() {
        return vehicleRequestType;
    }

    public void setVehicleRequestType(VehicleRequestType vehicleRequestType) {
        this.vehicleRequestType = vehicleRequestType;
    } 

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }
    
    public void addResolvedVehiclePartCondition(VehiclePart vehiclePart,VehicleCondition vehicleCondition ){
        if(vehiclePart != null && vehicleCondition != null && resolvedVehiclePartConditions != null){
            VehiclePartCondition vehiclePartCondition = new VehiclePartCondition();
            vehiclePartCondition.setCondition(vehicleCondition);
            vehiclePartCondition.setVehiclePart(vehiclePart);
            resolvedVehiclePartConditions.put(vehiclePartCondition.getVehiclePart(), vehiclePartCondition);
        }
    }

    public Map<VehiclePart, VehiclePartCondition> getResolvedVehiclePartConditions() {
        return resolvedVehiclePartConditions;
    }

    public void setResolvedVehiclePartConditions(Map<VehiclePart, VehiclePartCondition> resolvedVehiclePartConditions) {
        this.resolvedVehiclePartConditions = resolvedVehiclePartConditions;
    }
    

    @Override
    public String toString() {
        return vehicleRequestType.getValue(); //To change body of generated methods, choose Tools | Templates.
    }    
    
}
