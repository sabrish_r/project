/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.workqueue;

/**
 *
 * @author Sabrish
 */
public enum RequestStatus {
    REQUEST_OPEN("Request OPen"),
    REQUEST_RAISED("Request Raised"),
    REQUEST_PENDING("Request Pending"),
    REQUEST_ACCEPTED("Request Accepted"),
    REQUEST_CLOSED("Request Closed"),
    REQUEST_TRANSFFERED("Request Transfered"),
    REQUEST_DENIED("Request Denied");
    
         private String value;
        private RequestStatus(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
}
