/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.workqueue;

import com.neu.business.ErrorCode;
import com.neu.business.Response;
import java.util.ArrayList;

/**
 *
 *
 */
public class WorkQueue {
    
    private ArrayList<WorkRequest> workRequestList;

    public WorkQueue() {
        workRequestList = new ArrayList<>();
    }

    public ArrayList<WorkRequest> getWorkRequestList() {
        return workRequestList;
    }
    
    public Response addWorkRequest(WorkRequest workRequest){
        Response<WorkRequest> response = new Response<>();
        ErrorCode errorCode = checkIfWorkRequestAlreadyExist(workRequest);
        response.setErrorCode(errorCode);
        response.setResponse(workRequest);
        if(errorCode == ErrorCode.SUCCESS){
            workRequestList.add(workRequest);
        }
        return response;
    }
    
    private ErrorCode checkIfWorkRequestAlreadyExist(WorkRequest workRequest){
        
        if(workRequestList != null && workRequestList.size() > 0){
            if(workRequestList.contains(workRequest)){
                 return ErrorCode.ALREADY_EXIST;
            }
        }
        
        return ErrorCode.SUCCESS;
    }
    
    public boolean removeRequest(WorkRequest workRequest){
         if(workRequestList != null && workRequestList.size() > 0){
            if(workRequestList.contains(workRequest)){
                 workRequestList.remove(workRequest);
                 return true;
            }
        }
         
         return false;
    }
}