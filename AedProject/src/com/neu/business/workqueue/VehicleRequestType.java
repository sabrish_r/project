/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.workqueue;

/**
 *
 * @author Sabrish
 */
public enum VehicleRequestType {
    
    SERVICE("Service"),
    INSURANCE("Insurance"),
    CRITICAL("Critical");
    
      private String value;
       private VehicleRequestType(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
}
