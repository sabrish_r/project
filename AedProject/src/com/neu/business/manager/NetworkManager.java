/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.manager;

import com.neu.business.ErrorCode;
import com.neu.business.Response;
import com.neu.business.network.INetworkManager;
import com.neu.business.network.Network;
import java.util.ArrayList;

/**
 *
 * @author Sabrish
 */
public class NetworkManager implements INetworkManager{
    
    private ArrayList<Network> networkList;
    
    public NetworkManager(){
        networkList = new ArrayList<>();
    }
    
     @Override
      public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    @Override
    public Response createAndAddNetwork(String name) {
         Response<Network> response = new Response<>();
         ErrorCode errorCode = checkIfNetworkExist(name);
        if(errorCode == ErrorCode.SUCCESS){         
             Network network = new Network();
             network.setName(name);
             networkList.add(network);
             response.setResponse(network);
             response.setErrorCode(ErrorCode.SUCCESS);
        }else if(errorCode == ErrorCode.ALREADY_EXIST){
             response.setResponse(null);
             response.setErrorCode(ErrorCode.ALREADY_EXIST);
        }else{
             response.setResponse(null);
             response.setErrorCode(ErrorCode.FAILURE);
        }
       return response;
    }

    @Override
    public boolean deleteNetwork(Network network) {
        
        if(networkList != null){
            if(networkList.contains(network)){
                networkList.remove(network);
                return true;
            }
        }
        
        return false;
    }
    
    private ErrorCode checkIfNetworkExist(String name){
        if(networkList != null && networkList.size() > 0){
            for(Network network : networkList){
                if(network.getName().equals(name)){
                    return ErrorCode.ALREADY_EXIST;
                }
            }
        }
        return ErrorCode.SUCCESS;
    }
}
