/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.manager;

import java.util.Map;

/**
 *
 * @author Sabrish
 */
public interface IBroadcastObserver {
    
    public void onNotificationState(BroadcastMessage a_NotificationState , Map<BroadcastStateKey, Object> a_Parameters );
}
