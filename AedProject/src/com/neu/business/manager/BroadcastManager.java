/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.manager;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Sabrish
 */
public class BroadcastManager {
    
    private ArrayList<IBroadcastObserver> iBroadcastObservers;
    
    public BroadcastManager(){
        iBroadcastObservers = new ArrayList<>();
    }
    
    public void registerBroadcastObserver(IBroadcastObserver broadcastObserver){
        if(broadcastObserver != null && !iBroadcastObservers.contains(broadcastObserver)){
            iBroadcastObservers.add(broadcastObserver);
        }
    }
    
    public void unregisterBroadcastObserver(IBroadcastObserver broadcastObserver){
         if(broadcastObserver != null && iBroadcastObservers.contains(broadcastObserver)){
            iBroadcastObservers.remove(broadcastObserver);
        }
    }
    
    public void sendBroadCast(Map<BroadcastStateKey, Object> a_Parameters ){
        
        if(iBroadcastObservers != null && iBroadcastObservers.size() > 0){
            for(IBroadcastObserver broadcastObserver : iBroadcastObservers){
                broadcastObserver.onNotificationState(BroadcastMessage.BROADCAST, a_Parameters);
            }
        }
        
    }
}
