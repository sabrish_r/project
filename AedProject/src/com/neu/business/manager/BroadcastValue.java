/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.manager;

/**
 *
 * @author Sabrish
 */
public enum BroadcastValue {
    
    VEHICLE_USER_CREATED,
    VEHICLE_USER_UPDATED,
    VEHICLE_CREATED_APPOINTMENT,
    VEHICLE_REQUEST_ACCEPTED,
    SERVICE_CREATED_APPOINTMENT,
    VEHICLE_REQUEST_UPDATED,
    VEHICLE_STATUS_UPDATED
    
}
