/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.manager;

import com.neu.business.ErrorCode;
import com.neu.business.Response;
import com.neu.business.role.Role;
import com.neu.business.useraccount.User;
import com.neu.business.useraccount.UserAccount;
import com.neu.business.useraccount.UserType;
import java.util.ArrayList;

/**
 *
 * @author Sabrish
 */
public interface IUserAccountManager {
    
    public UserAccount login(String username, String password);
    public Response createUserAccount(String username, String password, UserType userType, User person, Role role);
    public void addUserAccount(UserAccount userAccount);
    public boolean removeUserAccount(UserAccount userAccount);
    public ErrorCode checkIfUserAccountExist(String name);
    public ArrayList<UserAccount> getUserAccountList();
}
