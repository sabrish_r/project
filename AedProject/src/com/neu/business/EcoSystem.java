/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business;

import com.neu.business.manager.NetworkManager;
import com.neu.business.network.INetworkManager;
import com.neu.business.organization.Organization;

/**
 *
 * @author Sabrish
 */
public class EcoSystem extends Organization{
    
    private IManagerDelegate iManagerDelegate;
    private static EcoSystem business;
    private NetworkManager networkManager;

    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkManager = new NetworkManager();
        iManagerDelegate = new ManagerDelegate();
    }
    
    public INetworkManager getNetworkManager(){
        return networkManager;
    }
    
    public IManagerDelegate getManagerDelegate(){
        return iManagerDelegate;
    }
}
