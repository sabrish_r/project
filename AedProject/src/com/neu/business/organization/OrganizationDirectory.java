/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.organization;

import com.neu.business.ErrorCode;
import com.neu.business.Response;
import java.util.ArrayList;
import com.neu.business.organization.Organization.Type;

public class OrganizationDirectory {

    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public Response createOrganization(Type type) {
        Response<Organization> response = new Response<>();
        ErrorCode errorCode = checkIfOrganisationExist(type);
        response.setErrorCode(errorCode);
        if (errorCode == ErrorCode.SUCCESS) {
            Organization organization = null;
            if (type == Organization.Type.ServiceCentreOrganization) {
                organization = new ServiceCentreOrganization(type.getValue());
            } else if (type == Organization.Type.MechanicOrganization) {
                organization = new MechanicOrganization(type.getValue());
            } else if (type == Organization.Type.VehicleOrganization) {
                organization = new VehicleOrganization(type.getValue());
            } else if (type == Organization.Type.AnalysisOrganisation) {
                organization = new AnalysisOrganisation(type.getValue());
            }
            if (organization != null) {
                organizationList.add(organization);
                response.setResponse(organization);
            }
        }
        return response;
    }

    private ErrorCode checkIfOrganisationExist(Type type) {
        if (organizationList != null && organizationList.size() > 0) {
            for (Organization organization : organizationList) {
                if (organization.getName().equals(type.getValue())) {
                    return ErrorCode.ALREADY_EXIST;
                }
            }
        }
        return ErrorCode.SUCCESS;
    }

}
