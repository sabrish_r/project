/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.organization;

import com.neu.business.employee.EmployeeDirectory;
import com.neu.business.employee.IEmployeeManager;
import com.neu.business.manager.IUserAccountManager;
import com.neu.business.useraccount.UserAccount;
import com.neu.business.useraccount.UserAccountDirectory;
import com.neu.business.workqueue.WorkQueue;
import com.neu.business.workqueue.WorkRequest;


public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private IEmployeeManager employeeDirectory;
    private IUserAccountManager userAccountDirectory;
    private int organizationID;
    private static int counter;
    
    public enum Type{
        AdminOrganization("Admin Organization"), VehicleOrganization("Vehicle Organization"), ServiceCentreOrganization("Service Centre Organization"), 
        MechanicOrganization("Mechanic Organization"), AnalysisOrganisation("Analysis Organisation");
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        ++counter;
    }

    
    public IUserAccountManager getUserAccountManager() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public IEmployeeManager getEmployeeManager() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    @Override
    public String toString() {
        return name;
    }
    
    public void deleteRequestFromAllAccount(WorkRequest workRequest){
       if(getWorkQueue().getWorkRequestList().contains(workRequest)){
            getWorkQueue().getWorkRequestList().remove(workRequest);
        }
       
       if(userAccountDirectory.getUserAccountList() != null && userAccountDirectory.getUserAccountList().size() > 0){
           for(UserAccount userAccount : userAccountDirectory.getUserAccountList()){
               userAccount.getWorkQueue().getWorkRequestList().remove(workRequest);
           }
       }
    }
}
