/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.enterprise;

import com.neu.business.Response;
import java.util.ArrayList;

/**
 *
 * @author Sabrish
 */
public interface IEnterpriseManager {
    
     public ArrayList<Enterprise> getEnterpriseList();
     public Response createAndAddEnterprise(String name, Enterprise.EnterpriseType type);
     public boolean deleteEnterprise(Enterprise enterprise);
    
}
