/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.enterprise;

import com.neu.business.ErrorCode;
import com.neu.business.organization.MechanicOrganization;
import com.neu.business.organization.Organization;
import com.neu.business.organization.OrganizationDirectory;
import com.neu.business.vehicle.Address;
import com.neu.business.workqueue.VehicleServiceRequest;
import com.neu.business.workqueue.WorkRequest;
import java.util.ArrayList;


public  class Enterprise extends Organization{

    private EnterpriseType enterpriseType;
    private OrganizationDirectory organizationDirectory;
    private Address address;
    
    public Enterprise(String name, EnterpriseType type) {
        super(name);
        this.enterpriseType = type;
        organizationDirectory = new OrganizationDirectory();
        address = new Address();
    }
    
    public enum EnterpriseType{
        LOCATION_CENTRE("Location Centre"),
        ANALYSIS("Analysis Centre")
        ;
        
        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }
    
    public boolean deleteWorkRequest(WorkRequest workRequest){
        
        if(getWorkQueue().getWorkRequestList().contains(workRequest)){
            getWorkQueue().getWorkRequestList().remove(workRequest);
        }
        
        for(Organization organization : getOrganizationDirectory().getOrganizationList()){
            organization.deleteRequestFromAllAccount(workRequest);
        }
        
        return false;
    }

    @Override
    public String toString() {
        return getName(); //To change body of generated methods, choose Tools | Templates.
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    
     public ErrorCode checkIfUserAccountExist(String name){
         if((getUserAccountManager().checkIfUserAccountExist(name)) == ErrorCode.SUCCESS){
             if(organizationDirectory != null && organizationDirectory.getOrganizationList() != null && organizationDirectory.getOrganizationList().size() > 0){
                 for(Organization organization : organizationDirectory.getOrganizationList()){
                     if(organization.getUserAccountManager().checkIfUserAccountExist(name) == ErrorCode.USERNAME_EXIST){
                        return ErrorCode.USERNAME_EXIST;  
                     }
                 }
             }
         }else{
             return ErrorCode.USERNAME_EXIST;
         }
         
         return ErrorCode.SUCCESS;
     }

     public ArrayList<WorkRequest> getAllAnalyticsRequest(){    
          if(organizationDirectory != null && organizationDirectory.getOrganizationList() != null && organizationDirectory.getOrganizationList().size() > 0){
                 for(Organization organization : organizationDirectory.getOrganizationList()){
                     if(organization instanceof MechanicOrganization){
                        return((MechanicOrganization)organization).getWorkQueue().getWorkRequestList();
                     }
                 }
             }
         return null;
     }
}
