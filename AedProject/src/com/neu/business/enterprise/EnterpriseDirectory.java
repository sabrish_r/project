/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business.enterprise;

import com.neu.business.ErrorCode;
import com.neu.business.Response;
import java.util.ArrayList;

public class EnterpriseDirectory  implements  IEnterpriseManager{
    
    private ArrayList<Enterprise> enterpriseList;

    public EnterpriseDirectory() {
        enterpriseList = new ArrayList<>();
    }

    @Override
    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }
    
    @Override
    public Response createAndAddEnterprise(String name, Enterprise.EnterpriseType type){
        Response<Enterprise> response = new Response<>();
        ErrorCode errorCode = checkIfEnterpriseExist(name);
        response.setErrorCode(errorCode);
         if(errorCode == ErrorCode.SUCCESS){
           Enterprise enterprise = new Enterprise(name, type);
           enterpriseList.add(enterprise);
           response.setResponse(enterprise);
         }
        
        return response;
    }

    @Override
    public boolean deleteEnterprise(Enterprise enterprise) {
        
        if(enterpriseList != null && enterpriseList.size() > 0 && enterpriseList.contains(enterprise)){
            enterpriseList.remove(enterprise);
            return true;
        }
        
        return false;
    }
    
     private ErrorCode checkIfEnterpriseExist(String name){
        if(enterpriseList != null && enterpriseList.size() > 0){
            for(Enterprise enterprise : enterpriseList){
                if(enterprise.getName().equals(name)){
                    return ErrorCode.ALREADY_EXIST;
                }
            }
        }
        return ErrorCode.SUCCESS;
    }
    
}
