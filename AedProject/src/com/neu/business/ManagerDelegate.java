/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.business;

import com.neu.business.DB4OUtil.DB4OUtil;
import com.neu.business.role.SysAdminRole;
import com.neu.business.employee.Employee;
import com.neu.business.enterprise.Enterprise;
import com.neu.business.manager.BroadcastManager;
import com.neu.business.manager.BroadcastStateKey;
import com.neu.business.manager.BroadcastValue;
import com.neu.business.manager.IBroadcastObserver;
import com.neu.business.manager.IUserAccountManager;
import com.neu.business.network.INetworkManager;
import com.neu.business.organization.Organization;
import com.neu.business.organization.VehicleOrganization;
import com.neu.business.useraccount.UserAccount;
import com.neu.business.useraccount.UserType;
import com.neu.business.vehicle.Vehicle;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Sabrish
 */
public class ManagerDelegate implements IManagerDelegate{
    
    private  EcoSystem ecoSystem;
    private BroadcastManager broadcastManager;
    
    public ManagerDelegate(){
        broadcastManager = new BroadcastManager();
    }
    
     private static DB4OUtil dB4OUtil = DB4OUtil.getInstance();
      public  EcoSystem configure(){
        
        ecoSystem = EcoSystem.getInstance();
        
        //Create a network
        //create an enterprise
        //initialize some organizations
        //have some employees 
        //create user account
        
        
          Employee employee = ecoSystem.getEmployeeManager().createEmployee("sysadmin");      
          ecoSystem.getUserAccountManager().createUserAccount("sysadmin", "sysadmin", UserType.ECOSYTEM_ADMIN,employee, new SysAdminRole());
        
        return ecoSystem;
    }

    @Override
    public EcoSystem configureEcoSystem() {
         configure();              
         return ecoSystem;
        
    }

    @Override
    public EcoSystem getEcoSystem() {
        if(ecoSystem == null){
            ecoSystem = dB4OUtil.retrieveSystem();
        }
        return ecoSystem;
    }

    @Override
    public IUserAccountManager getUserAccountManger() {
        return getEcoSystem().getUserAccountManager();
    }

    @Override
    public INetworkManager getNetworkManager() {
        if(getEcoSystem() != null){
            return getEcoSystem().getNetworkManager();
        }
        
        return null;
    }

   
    public ArrayList<UserAccount> getVehiclesWithIssue(Enterprise enterprise){
        ArrayList<UserAccount> vehicleAccounts = new ArrayList<>();
        if(enterprise != null){
            
            VehicleOrganization organization = null;
            for(Organization organization1 : enterprise.getOrganizationDirectory().getOrganizationList()){
                if(organization1 instanceof VehicleOrganization){
                    ArrayList<UserAccount> userAccounts = organization1.getUserAccountManager().getUserAccountList();
                    if(userAccounts != null && userAccounts.size() > 0){
                        for(UserAccount userAccount : userAccounts){
                            Vehicle vehicle = (Vehicle)userAccount.getPerson();
                              if(vehicle != null){
                                  //for(VehicleP)
                              }
                        }
                    }
                }
            }
            
        }
        
        return vehicleAccounts;
    }

    @Override
    public void sendBroadCast(Map<BroadcastStateKey, Object> a_Parameters) {
       if(broadcastManager != null){
           broadcastManager.sendBroadCast(a_Parameters);
       }
    }

    @Override
    public void registerBroadcastObserver(IBroadcastObserver broadcastObserver) {
       if(broadcastManager != null){
           broadcastManager.registerBroadcastObserver(broadcastObserver);
       }
    }

    @Override
    public void unregisterBroadcastObserver(IBroadcastObserver broadcastObserver) {
       if(broadcastManager != null){
           broadcastManager.unregisterBroadcastObserver(broadcastObserver);
       }
    }
}
